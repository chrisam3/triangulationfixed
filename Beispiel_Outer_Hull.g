# Tetrahedron Example

# lower points 
lp:=[[-1,-1,0],[3,-1,0]];
# upper points
up:=[[1,1,1],[1,-3,1]];
# join together
points:=Concatenation(lp,up);;
points:=points*[[1,0,0],[0,1,0],[0,0,2*Sqrt(2.)]];
# plot Tetrahedron with given points
t:=Tetrahedron();;
# normal vector for face 1 TODO is it correctly oriented? Else take n:=-n
n:=Crossproduct(points[2]-points[1],points[3]-points[1]);
n:=-n/Norm2(n);  
# calculate outer hull 
data:=OuterHull(t,points,1,n);
# get STL with correct normals
DrawSTLwithNormals(data[2],"test",points,data[4],[]);




## New Problematic Surface (Icosahedron_5_2)
Coord5_2:= [
[  0.6564310317,  0.0000000000,  0.0000000000 ],
[ -0.1052636106,  0.6479361631,  0.0000000000 ],
[ -0.1052636106, -0.1237447958,  0.6360098245 ],
[ -0.1052636106, -0.1237447958, -0.6360098245 ],
[ -0.1052636106, -0.6006698570,  0.2429341358 ],
[  0.7447524966, -0.6335243129,  0.7686652477 ],
[ -0.5760167887, -0.2002232857, -0.2429341358 ],
[ -0.7447524966,  0.6335243129, -0.7686652477 ],
[ -0.5760167887,  0.2767017754,  0.1501415529 ],
[  0.3654895674,  0.2474895917,  0.4858682716 ],
[  0.3654895674, -0.5241913672, -0.1501415529 ],
[  0.1856778537,  0.4004465713, -0.4858682716 ],
];;
ico:=Icosahedron();;
pr:=SetVertexCoordinates3D(ico,Coord5_2);;
data:=DrawSurfaceToSTL(ico,"Icosahedron_5_2",pr,true);;
t:=data[2]; 
points:=data[3];
Read("FixEdgeIntersection.g");
Read("outer_hull.g");
# Still Some edges intersect and thus outer hull programm does not work :(
has_line_intersection:=true;
while has_line_intersection do
has_line_intersection:=false;
for e1 in Edges(t) do
	for e2 in Edges(t) do
		calculate := LineSegmentIntersection(points{VerticesOfEdge(t,e1)},points{VerticesOfEdge(t,e2)});
		if e1 < e2 and calculate[1] = true and Size(Intersection(VerticesOfEdge(t,e1),VerticesOfEdge(t,e2))) = 0 then
			has_line_intersection:=true;
			t:=FixEdgeIntersection(t,e1,e2,points,calculate[2]);
			Add(points,calculate[2]);
			Print("Edge ", e1, " and Edge ",e2, " intersect at ", calculate[2], " 凸ಠ益ಠ)凸 \n");
		fi;
	od;
od;
od;
n:=Crossproduct(points[5]-points[4],points[6]-points[4]);
n:=-n/Norm2(n);  
# calculate outer hull 
data:=OuterHull(t,points,2,n);
# get STL with correct normals
DrawSTLwithNormals(data[2],"Icosahedron_5_2_normals",points,data[4],data[3]);

# New Minimal Counter Example
Read("FixEdgeIntersection.g");
Read("line_intersection.g");
points:=1.*[[0,0,0],[2,0,0],[1,0,0],[1,1,0],[1,-1,0],[3.,0.,0.],[-1.,0.,0.]];
faces:=[[1,2,4],[1,3,5],[2,3,5],[4,1,7],[1,7,5],[2,4,6],[2,5,6]];

t:=TriangularComplexByVerticesInFaces(faces);
p:=SetVertexCoordinates3D(t,points);;
data:=DrawSurfaceToSTL(t,"Icosahedron_5_2",p,true);;
pr:=rec();
pr.vertexCoordinates3D:=points;
for e1 in Edges(t) do
	for e2 in Edges(t) do
		calculate := LineSegmentIntersection(points{VerticesOfEdge(t,e1)},points{VerticesOfEdge(t,e2)});
		if e1 < e2 and calculate[1] = true and Size(Intersection(VerticesOfEdge(t,e1),VerticesOfEdge(t,e2))) = 0 then
			has_line_intersection:=true;
			t:=FixEdgeIntersection(t,e1,e2,points,calculate[2]);
			Add(points,calculate[2]);
			Print("Edge ", e1, " and Edge ",e2, " intersect at ", calculate[2], " 凸ಠ益ಠ)凸 \n");
		fi;
	od;
od;








## Problematic surface (works now)
## two edges intersect and hence the outer hull algorithm does not work
r := DrawChosenSurface("32",1000,[0]);   
t := r[2];
points := r[3];
for e1 in Edges(t) do
	for e2 in Edges(t) do
		calculate := LineSegmentIntersection(points{VerticesOfEdge(t,e1)},points{VerticesOfEdge(t,e2)});
		if e1 < e2 and calculate[1] = true and Size(Intersection(VerticesOfEdge(t,e1),VerticesOfEdge(t,e2))) = 0 then
			Print("Edge ", e1, " and Edge ",e2, " intersect at ", calculate[2], " 凸ಠ益ಠ)凸 \n");
		fi;
	od;
od;


## analogue counter example (works now 24.1.21)

points:=[[-1,1,0],[0,2,0],[0,0,0],[1,1,0],[0,1,1],[0,2,0],[0,0,0],[0,1,-1]];
VoF:=[[1,2,3],[2,3,4],[5,6,8],[5,7,8]];
s:=SimplicialSurfaceByVerticesInFaces(VoF);
pr:=SetVertexCoordinates3D(s,points);
DrawSurfaceToSTL(s,"MinimalCounterExample_1",pr,true);

## another problematic example (works now 24.1.21)

points:=[[0,0,0],[2,0,0],[1,3,0],[1,1,0],[0,4,0],[2,4,0]];
VoF:=[[1,2,3],[4,5,6]];
s:=SimplicialSurfaceByVerticesInFaces(VoF);
pr:=SetVertexCoordinates3D(s,points);
DrawSurfaceToSTL(s,"MinimalCounterExample_2",pr,true);

