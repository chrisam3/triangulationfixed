# Find outer triangle
# for a triangular complex with points find an outer face and
# a normal vector pointing outwards
FindOuterTriangle:=function(t,points)
	local i_max,px_max,i,eov,j_min,x_max,j_max,x_min,e,f,n,foe;
	# find point with maximal x coordinate
	i_max:=1;
	px_max:=points[i_max][1];
	for i in [2..Size(points)] do
		if points[i][1]>px_max then
			px_max:=points[i][1];
			i_max:=i;
		fi;
	od;
	eov:=EdgesOfVertex(t,i_max);
	eov:=List(eov,e->[e,DifferenceLists(VerticesOfEdge(t,e),[i_max])]);
	eov:=List(eov,e->[e[1],points[e[2][1]]-points[i_max]]);
	eov:=List(eov,e->[e[1],e[2]/MyNorm(e[2])]);
	# find edge with smallest absolute value in x-coordinate
	j_min:=1;
	x_min:=AbsoluteValue(eov[1][2][1]);
	for j in [2..Size(eov)] do
		if AbsoluteValue(eov[j][2][1]) < x_min then
			j_min:=j;
			x_min:=AbsoluteValue(eov[j][2][1]);
		fi;
	od;
	# found the edge
	e:=eov[j_min][1];
	foe:=FacesOfEdge(t,e);
	foe:=List(foe,f->[f,VerticesOfFace(t,f)]);
	foe:=List(foe,f->[f[1],Crossproduct(points[f[2][2]]-points[f[2][1]],points[f[2][3]]-points[f[2][1]])]);
	foe:=List(foe,f->[f[1],f[2]/MyNorm(f[2])]);
	# find edge with smallest absolute value in x-coordinate
	j_max:=1;
	x_max:=AbsoluteValue(foe[1][2][1]);
	for j in [2..Size(foe)] do
		if AbsoluteValue(foe[j][2][1]) > x_max then
			j_max:=j;
			x_max:=AbsoluteValue(foe[j][2][1]);
		fi;
	od;
	# found the face
	f:=foe[j_max][1];
	n:=foe[j_max][2];
	if foe[j_max][2][1]<0. then
		n:=-n;
	fi;
	return [f,n];
end;;

# group Group(()) or subgroup of the Automorphism group of t
PrintableOuterHull:=function(t,points,name,eps,shift_param,group)
	local data,f,n,order_data,unramified_data,unram_surf,unram_points;
	# the first step can be parallelized by using true instead of false
	# and fixing each triangle in parallel
	data:=calculate_intersections(VerticesOfFaces(t),points,false,group);
	points:=data[1];;
	t:=TriangularComplexByVerticesInFaces(data[2]);
	data:=FindOuterTriangle(t,points);
	f:=data[1];
	n:=data[2];
	data:=OuterHull(t,points,f,n);
	t := ShallowCopy(data[2]);
	if Size(RamifiedEdges(t))>0 then
		order_data := OrderRamifiedEdges(t,ShallowCopy(data));
		unramified_data := FixRamPath(t,order_data,data,ShallowCopy(points),shift_param);
		t := unramified_data[1];
		points := unramified_data[2];
		DrawSTLwithNormals(t,name,points,data[4],[]); 
		return [data[1],t,points,data[4]];
	fi;
	DrawSTLwithNormals(t,name,points,data[4],[]); 
	return [data[1],t,points,data[4]];
end;;