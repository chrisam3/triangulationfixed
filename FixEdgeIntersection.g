FixEdgeIntersection:=function(t,e1,e2,points, point)
	local vof, foe1,foe2,voe1,voe2,newpoint,new_vof,f,i,found_point;
	found_point:=false;
	for i in [1..Size(points)] do
		if MyNorm(points[i]-point) < Float(10^(-6)) then 
			newpoint:=i;
			found_point:=true;
			break;
		fi;
	od;
	if not found_point then
		newpoint:=NumberOfVertices(t)+1;
	fi;
	voe1:=VerticesOfEdge(t,e1);
	if not newpoint in voe1 then
		foe1:=FacesOfEdge(t,e1);
	else
		foe1:=[];
	fi;
	voe2:=VerticesOfEdge(t,e2);
	if not newpoint in voe2 then
		foe2:=FacesOfEdge(t,e2);
	else
		foe2:=[];
	fi;
	
	new_vof:=[];
	for f in Faces(t) do
		vof:=VerticesOfFace(t,f);
		if f in foe1 then
			Add(new_vof,Union([newpoint],Difference(vof,[voe1[1]])));
			Add(new_vof,Union([newpoint],Difference(vof,[voe1[2]])));
		elif f in foe2 then
			Add(new_vof,Union([newpoint],Difference(vof,[voe2[1]])));
			Add(new_vof,Union([newpoint],Difference(vof,[voe2[2]])));
		else
			Add(new_vof,vof);
		fi;
	od;
	return TriangularComplexByVerticesInFaces(new_vof);
end;;