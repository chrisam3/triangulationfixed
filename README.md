# TriangulationFixed

# Authors: 

Christian Amend, Tom Goertzen

## Literature

- [1] As-exact-as-possible repair of unprintable STL files by Marco Attene

## Description

Create new triangulation of self-intersecting surfaces such that the new generated surface has no self-intersection. 
Calculate outer hull and new surface after the previous steps.

## TODOs

### Comments/Description

- [] Add Comments in animating.gi
- [] Write Latex-file about how the mathematical ideas work
- [] Add Autodoc comments in animating.gd
- [] Write testfile (could be also done via Autodoc in animating.gd)

### Programming

- [x] create TriangularComplex from new Triangulation (necessary for next step)
- [x] calculate outer hull with Algorithm from [1]

## Examples

- Icosahedron_1_1 successfully retriangulated and printed
- Icosahedron_3_2 triangulated but still need to calculate outer hull 

## Further ideas

- Add "volume" at zero thickness edges (Edge with more that two triangles) see 
- Automatically, find out right orientation for one face (necessary for Algorithm from [1])