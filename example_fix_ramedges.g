Read("triangulation.gd");
Read("triangulation.gi");
Read("my_triangle_fix.g"); 

# now shift the right faces
# find faces with same vertex v as f (3)
UmbrellaPathFaceVertex:=function(t,f,v,data,points)
	local faces,new_face,edges,edge,old_face;
	faces:=[];
	new_face:=f;
	edge:=Intersection(EdgesOfVertex(t,v),EdgesOfFace(t,new_face))[2];
	while not new_face in faces do
		Add(faces,new_face);
		# this way you only find one butterfly part of the ramified edge, right?
		# so it can be seperated from the other part later
		old_face:=new_face;
		if IsRamifiedEdge(t,edge) then
			new_face:=UpwardContinuation(t,edge,points,new_face,data[4][new_face])[1];
		else
			new_face:=Difference(FacesOfEdge(t,edge),[new_face])[1];
		fi;
		edge:=Difference(Intersection(EdgesOfVertex(t,v),EdgesOfFace(t,new_face)),EdgesOfFace(t,old_face))[1];
	od;
	return faces;
end;;

UmbrellaComponents := function(t,v)
	local u_comps, v_faces, v_edges, edges_of_faces, f, e, fa, edges, avaiable_edges, comp, connection, cur_edges;;
	
	u_comps := [];
	v_faces := ShallowCopy(FacesOfVertex(t,v));
	v_edges := ShallowCopy(EdgesOfVertex(t,v));
	edges_of_faces := [];
	for f in v_faces do
		 edges := ShallowCopy(EdgesOfFace(t,f));
		 edges_of_faces[f] := Intersection(edges,v_edges);
	od;
	while v_faces <> [] do
		fa := v_faces[1];
		Remove(v_faces,Position(v_faces,fa));
		comp := [fa];
		cur_edges := [edges_of_faces[fa][1]];
		
		available_edges := Unique(Flat(ShallowCopy(edges_of_faces)));
		# so that we dont land in an infinite loop
		Remove(available_edges,Position(available_edges,edges_of_faces[fa][2]));
		
		while Intersection(cur_edges,available_edges) <> [] do
			# while we can still reach a face on our side of the umbrella, continue
			for f in v_faces do
				connection := Intersection(edges_of_faces[f],cur_edges);
				if connection <> [] then
					Add(comp,f);
					cur_edges := Union(cur_edges,edges_of_faces[f]);
					Remove(v_faces,Position(v_faces,f));
					for e in connection do
						if e in available_edges then
							Remove(available_edges,Position(available_edges,e));
						fi;
					od;
				fi;
			od;
		od;
		Add(u_comps,comp);
	od;
	return u_comps;
end;;

ChooseIsolatedVertex:=function(t,e,points,data)
	local voe,foe;
	voe:=VerticesOfEdge(t,e);
	foe:=FacesOfEdge(t,e);
	if IsSubset(Set(foe),Set(UmbrellaPathFaceVertex(t,foe[1],voe[1],data,points))) then
		return voe[1];
	fi;
	return voe[2];
end;;

FixInnerRamEdge:=function(t,e,points,data,shift_param)
	local v,data_fan,data_fix,alpha,vC,n,vec,v_alpha,f,w,index_f,index_ff,points_fix,t_fix,MyNormal,VoE,VoF,v_index,w_index,v_faces,w_faces,i;
	f:=FacesOfEdge(t,e)[1];

	#halber winkel
	data_fan:=CalculateFan(t,e,points,f,data[4][f]);
	index_f:=0;
	for i in [1..Size(data_fan)] do
		if data_fan[i][1]=f then
			index_f:=i;
		fi;
	od;
	index_ff:=(index_f mod (Size(data_fan))) + 1;

	# in data_fan[4][4] ist der relative winkel zu face 3
	alpha:=data_fan[index_ff][4];
	# halbiere alpha
	alpha:=alpha/2;
	
	

	vC:=points;
	MyNormal:=data[4][index_f];
	VoE:=VerticesOfEdge(t,e);
	n:=vC[VoE[2]]-vC[VoE[1]];
	n:=n/MyNorm(n);
	vec:=data_fan[index_f][3];

	# wir überprüfen noch in welche richtung wir vec rotieren müssen
	if Determinant(1.*[vec,n,MyNormal]) < 0. then
		v_alpha:=Rotate_Vector_Axis(vec,n,alpha);
	else
		v_alpha:=Rotate_Vector_Axis(vec,-n,alpha);
	fi;

	# normieren

	v_alpha:=v_alpha/Norm2(v_alpha);

	
	v:=VoE[1];
	w:=VoE[2];

	points_fix:=points;
	Add(points_fix,points[v]+shift_param*v_alpha);
	v_index:=Size(points);

	Add(points_fix,points[w]+shift_param*v_alpha);
	w_index:=Size(points);

	v_faces:=UmbrellaPathFaceVertex(t,f,v,data,points); 
	w_faces:=UmbrellaPathFaceVertex(t,f,w,data,points); 

	VoF:=List(VerticesOfFaces(t),i->ShallowCopy(i));

	for f in v_faces do
		RemoveSet(VoF[f],v);
		AddSet(VoF[f],v_index);
	od;
	for f in w_faces do
		RemoveSet(VoF[f],w);
    	AddSet(VoF[f],w_index);
	od;

	t_fix:=TriangularComplexByVerticesInFaces(VoF);


	return [t_fix,points_fix];
end;;



FixOuterRamEdge:=function(t,e,points,data,shift_param,not_split)
	local v,data_fan,data_fix,alpha,vC,n,vec,v_alpha,f,w,index_f, targ, index_ff,points_fix,t_fix,MyNormal,VoE,VoF,v_index,w_index,v_faces,w_faces,i;
	f:=FacesOfEdge(t,e)[1];

	#halber winkel
	data_fan:=CalculateFan(t,e,points,f,data[4][f]);
	index_f:=0;
	for i in [1..Size(data_fan)] do
		if data_fan[i][1]=f then
			index_f:=i;
		fi;
	od;
	index_ff:=(index_f mod (Size(data_fan))) + 1;

	# in data_fan[4][4] ist der relative winkel zu face 3
	alpha:=data_fan[index_ff][4];
	# halbiere alpha
	alpha:=alpha/2;
	
	

	vC:=points;
	MyNormal:=data[4][index_f];
	VoE:=VerticesOfEdge(t,e);
	n:=vC[VoE[2]]-vC[VoE[1]];
	n:=n/MyNorm(n);
	vec:=data_fan[index_f][3];

	# wir überprüfen noch in welche richtung wir vec rotieren müssen
	if Determinant(1.*[vec,n,MyNormal]) < 0. then
		v_alpha:=Rotate_Vector_Axis(vec,n,alpha);
	else
		v_alpha:=Rotate_Vector_Axis(vec,-n,alpha);
	fi;

	# normieren

	v_alpha:=v_alpha/Norm2(v_alpha);

	
	v:=VoE[1];
	w:=VoE[2];
	
	
	targ := points[data_fan[index_f][2]];
	
	points_fix:=points;
	
    if v <> not_split then
        Add(points_fix,points[v]+shift_param*v_alpha);
	    v_index:=Size(points);
        v_faces:=UmbrellaPathFaceVertex(t,f,v,data,points); 
    fi;
	
    if w <> not_split then
        Add(points_fix,points[w]+shift_param*v_alpha);
	    w_index:=Size(points);
        w_faces:=UmbrellaPathFaceVertex(t,f,w,data,points);
    fi;


	VoF:=List(VerticesOfFaces(t),i->ShallowCopy(i));

    if v <> not_split then
        for f in v_faces do
		    RemoveSet(VoF[f],v);
		    AddSet(VoF[f],v_index);
	    od;
    fi;
	
    if w <> not_split then
	    for f in w_faces do
		    RemoveSet(VoF[f],w);
		    AddSet(VoF[f],w_index);
	    od;
    fi;

	t_fix:=TriangularComplexByVerticesInFaces(VoF);


	return [t_fix,points_fix];
end;;









# NEW ############################################################################################################

AdjacentFaces:=function(surf,face)
	local f_edges, edges, adj, es, f;
	f_edges := ShallowCopy(EdgesOfFace(surf,face));
	edges := ShallowCopy(EdgesOfFaces(surf));
	adj := [face];
	for es in edges do
		f := Position(edges,es);
		if f <> face and Intersection(es,f_edges) <> [] then
			Add(adj,f);
		fi;
	od;	
	
	return adj;
end;;


# Potential data loss if not all points are used anymore in surf
Normals:=function(surf,points)
    local f, faces, Norms, verts, c_verts;
    
    faces := Faces(surf);
    Norms := [];
    for f in faces do
        verts := ShallowCopy(VerticesOfFace(surf,f));
        c_verts := [points[verts[1]],points[verts[2]],points[verts[3]]];
        Norms[f] := Crossproduct(c_verts[1]-c_verts[2],c_verts[1]-c_verts[3])/Norm2(Crossproduct(c_verts[1]-c_verts[2],c_verts[1]-c_verts[3]));
    od;
    return Norms;
end;;

NormalsFromCoords:=function(Coords)
    local f, faces, Norms, verts, c_verts;
    Norms := [];
    for f in Coords do
        Norms[Position(Coords,f)] := f[4];
    od;
    return Norms;
end;;

FixNormals:=function(Coords)
    local f, faces, norm, c_verts, eps;
    eps := 1./10^6;
    for f in Coords do
        c_verts := ShallowCopy([f[1],f[2],f[3]]);
        norm := Crossproduct(c_verts[1]-c_verts[2],c_verts[1]-c_verts[3])/Norm2(Crossproduct(c_verts[1]-c_verts[2],c_verts[1]-c_verts[3]));
        if FlGeq(norm*f[4],0.,eps) then
        	f[4] := norm;
        else
        	f[4] := -norm;
        fi; 
    od;
    return Coords;
end;;

ConvertDataFormatPtC:=function(surf,points,normals)
    local faces, f, Coords, verts, c_verts;

    faces := Faces(surf);
    Coords := [];
    for f in faces do
        Coords[f] := [];
        verts := ShallowCopy(VerticesOfFace(surf,f));
        c_verts := [points[verts[1]],points[verts[2]],points[verts[3]]];

        Coords[f][1] := c_verts[1];
        Coords[f][2] := c_verts[2];
        Coords[f][3] := c_verts[3];
        Coords[f][4] := normals[f];
        Coords[f][5] := verts;
    od;
    return Coords;
end;;

ReadSTL:=function(fileName)
	# reads a file from the current dir
	local surf, file, name, r, r2, eps, filesepr, endsign, normal, data, normals, points, test, i,j, index, verts, coords, input, Coords;
	eps := 1./10^6;
	filesepr := SplitString(fileName, ".");
        name := filesepr[1];
        file := Filename( DirectoryCurrent(), Concatenation(name,".stl") );
        points := [];
        Coords:=[];
        i := 1;
       	# test file name
	if IsReadableFile(file) then
		
        	input := InputTextFile(file);
		r := ReadLine(input);
		Print(r);
		endsign := SplitString(ShallowCopy(r), " ")[1];
		
		while not endsign = "endsolid" do
			
			
			r := ReadLine(input);
			r2 := SplitString(ShallowCopy(r), " ");
			endsign := r2[1];
			if not endsign = "endsolid" then
				Coords[i]:=[];
				# TODO:  maybe find way to round less?
				
				normal := [Float(r2[3]),Float(r2[4]),Float(r2[5])];
				Coords[i][4]:=normal;
				
				r := ReadLine(input);
				
				j := 1;
				verts := [];
				while j < 4 do
					r := ReadLine(input);
					r2 := SplitString(ShallowCopy(r), " ");
					coords := [Float(r2[2]),Float(r2[3]),Float(r2[4])];
					
					test := ShallowCopy(points);
					Add(test,coords);
					if Length(NumericalUniqueListOfLists(test,eps)) > Length(points) then
						Add(points,coords);
						index := Length(points); 
					else
						index := NumericalPosition(points,coords,eps);
					fi;
					verts[j] := index;
					Coords[i][j] := coords;
					j := j+1;
				od;
				Coords[i][5] := verts;
				r := ReadLine(input);
				r := ReadLine(input);
				i := i + 1;
			fi;
			
		od;
		
	else
		Print("file does not exist");
		return 0;
	fi;
	
	faces := [1..Length(Coords)];
	data := SimplicialSurfaceFromCoordinates([Coords,faces],eps);
	return [data[1],Coords,points];
end;


ConvertDataFormatCtP:=function(surf,Coords)
    local f, faces, points, normals, c_p, normal, v;

    faces := Faces(surf);
    points := [];
    normals := [];

    for f in faces do
        c_p := [Coords[f][1],Coords[f][2],Coords[f][3]];
        normal := Coords[f][4];
        v := Coords[f][5];
        
        points[v[1]] := c_p[1];
        points[v[2]] := c_p[2];
        points[v[3]] := c_p[3];
        normals[f] := normal;
    od;
    return [points,normals];
end;;

FixVertOfOuterRamEdge:=function(t,e,Coords,points,data,shift_param,not_split)
	local v,data_fan,data_fix,alpha,verts_e, vC,n,v_p, p_f, n_f, n_ff,f, ff, vec,v_alpha,w,index_f, verts_f, verts_ff, index_ff,points_fix,t_fix,MyNormal,VoE,VoF,fa, u, p, v_index,w_index,v_faces,w_faces,i;
	f:=FacesOfEdge(t,e)[1];

	
	data_fan:=CalculateFan(t,e,points,f,data[4][f]);
    

	index_f:=0;
	for i in [1..Size(data_fan)] do
		if data_fan[i][1]=f then
			index_f:=i;
		fi;
	od;
	index_ff:=(index_f mod (Size(data_fan))) + 1;

    	ff := data_fan[index_ff][1];
    	verts_e := ShallowCopy(VerticesOfEdge(t,e));
    	verts_f := VerticesOfFace(t,f);       
    	verts_ff := VerticesOfFace(t,ff);

    	# have to check direction -> so pick correct faces to change : DONE
	
    	n_f := Filtered(verts_f,x-> not x in verts_e)[1];
   	n_ff := Filtered(verts_ff,x-> not x in verts_e)[1];
	
    	v := verts_e[1];
    	if v in not_split then
    		v := verts_e[2];
    	fi;
    	
    	points_fix:=points;
    	
	p := points[v];
	v_p := p + 0.5*shift_param*(points[n_f]-p) + 0.5*shift_param*(points[n_ff]-p);

	    	
	Add(points_fix,v_p);
	v_index:=Size(points_fix);

	    
    	# need to change all verts on umbrella
	    	
	    
    	u := UmbrellaPathFaceVertex(t,f,v,data,points);

    	for fa in u do
    		if v in Coords[fa][5] then
		    	p_f := Position(Coords[fa][5],v);
		    	Coords[fa][p_f] := v_p;
		    	Coords[fa][5][p_f] := v_index;
		fi;
    	od;
    	
	return [Coords,points_fix,[v]];
end;;

FixRamVert:= function(surf,p,Coords,points,data,shift_param)
	local coord_p,f, fp, pu, verts, umbrella_verts, points_fix, umb, p_index, coord_p_new, vec, p_f, fa, u, i;
	
	# fix ramified vertex
	
	coord_p := points[p];
	umb := UmbrellaComponents(surf,p);
	while umb <> [] do 
		umbrella_verts := [];
		u := umb[1];
		f := u[1];
		
		i := 1;
		
		# find vertices in local umbrella that are not p
		for fp in u do
			verts := ShallowCopy(VerticesOfFace(surf,fp));
			Remove(verts,p);
			umbrella_verts[i] := verts;
			i := i + 1;
		od;
		umbrella_verts := DuplicateFreeList(Flat(umbrella_verts));
		
		# take averaged direction of these vertices
		vec := [0.,0.,0.];
		for pu in umbrella_verts do
			vec := vec - coord_p + points[pu];
		od;
		vec := 1./Length(umbrella_verts)*shift_param*vec;
		coord_p_new := coord_p + vec;	
		Add(points,coord_p_new);
		p_index := Length(points);
		
		# Change the coords of all vertices on the local umbrella to unramify p
		
		
		for fa in u do
	    		if p in Coords[fa][5] then
			    	p_f := Position(Coords[fa][5],p);
			    	Coords[fa][p_f] := coord_p_new;
			    	Coords[fa][5][p_f] := p_index;
			fi;
		od;
		
		
		
		Remove(umb,Position(umb,u));
	od;
	points_fix := ShallowCopy(points);
	return [Coords,points_fix,[p]];
end;;


FixVertOfInnerRamEdge:=function(t,e,Coords,points,data,shift_param,not_split)
	local v,data_fan,data_fix, verts_e, n_f, n_ff, p_f, v_p, fa, alpha,vC,n,vec, verts_f, verts_ff, v_alpha,f,w,u,p, ff, initial_f, initial_ff, normal, index_f,index_ff,points_fix,t_fix,MyNormal,VoE,VoF,v_index,w_index,v_faces,w_faces,i;
	initial_f:=FacesOfEdge(t,e)[1];

	#halber winkel
	data_fan:=CalculateFan(t,e,points,initial_f,data[4][initial_f]);
    

	index_f:=0;
	for i in [1..Size(data_fan)] do
		if data_fan[i][1]=initial_f then
			index_f:=i;
		fi;
	od;
	index_ff:=(index_f mod (Size(data_fan))) + 1;

    	initial_ff := data_fan[index_ff][1];
    	verts_e := VerticesOfEdge(t,e);
    	

    	# have to check direction TODO fix orientation a bit more
	
	normal:=data[4][index_f];
	n:=points[verts_e[2]]-points[verts_e[1]];
	n:=n/MyNorm(n);
	vec:=data_fan[index_f][3];
    	
	
    	v := verts_e[1];
    	if v in not_split then
    		v := verts_e[2];
    	fi;
    	
    	points_fix:=points;
	
	
	if Determinant([vec,n,normal]) < 0. then
			f := initial_f;
			ff := initial_ff;
	else
		f := initial_f;
		ff := initial_ff;
	fi;
	p := points[v];
	
	verts_f := VerticesOfFace(t,f);       
	verts_ff := VerticesOfFace(t,ff);
	n_f := Filtered(verts_f,x-> not x in verts_e)[1];
	n_ff := Filtered(verts_ff,x-> not x in verts_e)[1];
	  	
	v_p := p + 0.5*shift_param*(points[n_f]-p) + 0.5*shift_param*(points[n_ff]-p);
	

	Add(points_fix,v_p);
	v_index:=Length(points_fix);
	    
    	# need to change all verts on umbrella path
    	
    	u := UmbrellaPathFaceVertex(t,f,v,data,points);

    	for fa in u do
    		if v in Coords[fa][5] then
		    	p_f := Position(Coords[fa][5],v);
		    	Coords[fa][p_f] := v_p;
		    	Coords[fa][5][p_f] := v_index;
		fi;
	od;

    	

	return [Coords,points_fix,[v]];
end;;



InnerRemEdge:= function(e,edges,verts_of_es)
	local in1, in2, vert1, vert2, q;
	in1 := false;
	in2 := false;
	vert1 := verts_of_es[e][1];
	vert2 := verts_of_es[e][2];
	for q in edges do
		if q <> e then
			if vert1 in verts_of_es[q] then
				in1 := true;
			elif vert2 in verts_of_es[q] then
				in2 := true;
			fi;
		fi;
	od;
	if in1 and in2 then
		return true;
	else
		return false;
	fi;
end;;

# only works with simple paths now
ChooseStartOfRamPath:=function(e,verts_of_e)
	local delet_e, inner, l, q, delet1, delet2, vert1, vert2;
    inner := [];
    i := 1;
	delet_e := ShallowCopy(e);
	for l in e do
        inner[l] := false;
		if InnerRemEdge(l,e,verts_of_e) then
			Remove(delet_e,Position(delet_e,l));
            inner[l] := true;
		fi;
        i := i+1;
	od;
	
	Flat(delet_e);
	if delet_e = [] then
		# ram edges form circle
		return [e,true,inner];
	else
		return [delet_e,false,inner];
	fi;
end;;

DetectCircle:=function(Edges,VertsOfEdges,start,next)
	local l, traversedVerts, leftVerts, leftEdges, reset, start_e, cur_e, cur_vs, n_v;
	reset := false;
	traversedVerts := [];
	leftVerts := Unique(Flat(ShallowCopy(VertsOfEdges)));
	leftEdges := ShallowCopy(Edges);
	start_e := start[1];
	cur_e := ShallowCopy(start_e);
	cur_vs := start[2];
	
	while leftVerts <> [] and leftEdges <> [] do
		for l in Edges do
			n_v := Intersection(cur_vs,VertsOfEdges[l],leftVerts);
			if n_v <> [] and l <> cur_e then
				if l = start_e then
					return true;
				fi;
				
				Remove(leftVerts,Position(leftVerts,n_v[1]));
				if cur_e in leftEdges then
					Remove(leftEdges,Position(leftEdges,cur_e));
				fi;
				cur_e := l;
				cur_vs := VertsOfEdges[l];
				reset := false;
			fi;
		od;
		if reset then
			return false;
		fi;
		l := start_e;
		reset := true;
	od;
	
	return false;
end;; 

OrderPath:=function(RamEdges,VertsOfRamEdges,start,path)
	local found, cur_verts, coincident_vert, circle_in_direction, q, c_numb, p, l, i, numb, next, next_verts, next_edges, next_edge_verts, comp;
	numb := 0;
	c_numb := 0;
	next := [];
	l := start[1];
	cur_verts := start[2];
	coincident_vert := start[3];
	
	# find next edges, make sure to not go backwards
	# TODO: fix circles
	for q in RamEdges do
		if ((cur_verts[1] in VertsOfRamEdges[q] and cur_verts[1] <> coincident_vert) or (cur_verts[2] in VertsOfRamEdges[q] and cur_verts[2] <> coincident_vert)) and not q in Flat(path) and not q = l then
				
				
				circle_in_direction := DetectCircle(RamEdges,VertsOfRamEdges,start,q);
				if not (circle_in_direction and c_numb > 0) then
					numb := numb + 1;
					if circle_in_direction then
						c_numb := c_numb + 1;
					fi;
					next[numb] := q;
				fi;
				
		fi;
		
	od;
	# check along which vert we are going
	if numb > 0 then
		if cur_verts[1] in VertsOfRamEdges[next[1]] then
			coincident_vert := cur_verts[1];
		else
			coincident_vert := cur_verts[2];
		fi;
	else
		coincident_vert := 0;
	fi;
	i := 0;
	comp := [];
	if numb = 1 then
		p := next[1];
		next_edges := ShallowCopy(Flat(DuplicateFreeList(RamEdges)));
		Unbind\[\](next_edges,Position(next_edges,p));
			
		next_verts := VertsOfRamEdges[p];
		next_edge_verts := ShallowCopy(VertsOfRamEdges);
		Unbind\[\](next_edge_verts,p);
		
		path[Length(path)+1] := l;
		OrderPath(next_edges,next_edge_verts,[p,next_verts,coincident_vert],path);
	elif numb = 0 then
		path[Length(path)+1] := l;
	else
		path[Length(path)+1] := l;
		for p in next do
			i := i + 1;
			
			next_edges := ShallowCopy(Flat(DuplicateFreeList(RamEdges)));
			Unbind\[\](next_edges,Position(next_edges,p));
			
			next_verts := VertsOfRamEdges[p];
			next_edge_verts := ShallowCopy(VertsOfRamEdges);
			Unbind\[\](next_edge_verts,p);
			
			if next_edges <> [] then
				comp[i] := OrderPath(next_edges,next_edge_verts,[p,next_verts,coincident_vert],[]);
			else
				comp[i] := [p];
			fi;
		od;
		
		if Length(Filtered(Flat(ShallowCopy(comp)),x-> not x in path)) = Length(Flat(ShallowCopy(comp))) then
			path[Length(path)+1] := comp;
		fi;
	fi;
	
	return path;
end;;




OrderRamifiedEdges:=function(surf, data)
	local e,VertsOfRamEdges, coincident_vert, path, isolated_edges, cur_path, found, l, q, i, cur_verts, info, pos, curr_verts;
	e:=ShallowCopy(RamifiedEdges(surf));
	VertsOfRamEdges := [];
	
	for l in e do
		VertsOfRamEdges[l] := VerticesOfEdge(surf,l);
	od;
	
	info := ChooseStartOfRamPath(e,VertsOfRamEdges);
	
	l := info[1][1];
	isolated_edges := [];
	path := [];
	cur_verts := [];
	i := 1;
	j := 1;
	cur_path := [l];
	coincident_vert := 0;
	
	while e <> [] do
		found := false;
		cur_verts := ShallowCopy(VertsOfRamEdges[l]);
		#VertsOfRamEdges[l] := [];
		cur_path := OrderPath(e,VertsOfRamEdges,[l,cur_verts,0],[]);
		path[i] := ShallowCopy(cur_path);
		
		cur_path := Flat(cur_path);
		for k in cur_path do
			Remove(e,Position(e,k));
		od;
		if e <> [] then
			l := e[1];
		fi;
		i := i + 1;
	od;
    return [path,info[2],info[3]];
end;;


FixRamPath := function(surf,order,data,points,shift_param)
	local l, comp, path, not_split, Coords, data_fix, inner, is_circle, points_fix, s_data, verts_current, verts_comp, same, e, s,t;
	not_split := [];
	is_circle := order[2];
	inner := order[3];
    	Coords := ConvertDataFormatPtC(surf,points,data[4]);
	for comp in order[1] do
		path := comp;
		not_split := [];
		for i in [1..Length(path)] do
			e:=path[i];
			# TODO: ADAPT TO NEW DATA TYP
			if IsInt(e) then
				if inner[e] or is_circle then			
					data_fix:=FixVertOfInnerRamEdge(surf,e,Coords,points,data,shift_param,not_split);
					Coords:=data_fix[1];
					points:=data_fix[2];
					not_split := data_fix[3];
				elif Length(path) = 1 then
					verts_current := VerticesOfEdge(surf,path[i]);
					
					same := ChooseIsolatedVertex(surf,e,points,data);
					Add(not_split,same);
					
					data_fix := FixVertOfOuterRamEdge(surf,e,Coords,points,data,shift_param,not_split);
		      			Coords:=data_fix[1];
					points:=data_fix[2];
				else
				    	verts_current := VerticesOfEdge(surf,path[i]);
				    	if i = 1 then
				     		verts_comp :=  VerticesOfEdge(surf,Flat(ShallowCopy(path))[i+1]);
				  	else
				   		verts_comp :=  VerticesOfEdge(surf,Flat(ShallowCopy(path))[i-1]);
					fi;
				 	# vertex which does not need to be split
				 	same := Filtered(verts_current,x-> not x in verts_comp)[1];
				 	Add(not_split,same);
					data_fix := FixVertOfOuterRamEdge(surf,e,Coords,points,data,shift_param,not_split);
		      			Coords:=data_fix[1];
					points:=data_fix[2];
					not_split := data_fix[3];
				fi;
			else
				path := e;
				i := 1;
			fi;
	    	od;
	    	
	od;
	s_data := SimplicialSurfaceFromChangedCoordinates([Coords,surf],1./10^6);
	surf := s_data[1];
	points_fix := points;
    	return [surf,points_fix,Coords];
end;;


FixRamIntersection := function(surf,order,info,data,points,Coords,shift_param)
	local l, int_v, branches, not_split, data_fix, b;
	int_v := info[1];
	branches := info[2];
	not_split := [];
	for b in branches do
		data_fix := FixVertOfOuterRamEdge(surf,b[1],Coords,points,data,shift_param,not_split);
		Coords:=data_fix[1];
		points:=data_fix[2];
		if Length(b[2]) > 1 then
			# the path continues in this direction
			# maybe test this at some point to be safe
			data_fix := FixRamPathRec(surf,[b[2],order[2],order[3]],data,points,Coords,shift_param);
			points := data_fix[1];
			Coords := data_fix[2];
		fi;
	od;
	return [points,Coords];
end;;

FixRamPathRec := function(surf,order,data,points,Coords,shift_param)
	local l, comp, path, not_split, data_fix, inner, is_circle, points_fix, s_data, branches, branch, verts_current, verts_comp, same, info,j, e, v,len, s,t;

	is_circle := order[2];
	inner := order[3];
	for comp in order[1] do
		path := comp;
		not_split := [];
		if IsInt(path) then
			if path in Edges(surf) then
				if inner[path] or is_circle then			
						data_fix:=FixVertOfInnerRamEdge(surf,path,Coords,points,data,shift_param,not_split);
						Coords:=data_fix[1];
						points:=data_fix[2];
						not_split := data_fix[3];
				else
						verts_current := VerticesOfEdge(surf,path);
						
						same := ChooseIsolatedVertex(surf,path,points,data);
						Add(not_split,same);
						data_fix := FixVertOfOuterRamEdge(surf,path,Coords,points,data,shift_param,not_split);
			      			Coords:=data_fix[1];
						points:=data_fix[2];
			
				fi;
			else 
				Print("mistake somewhere");
			fi;
			
		else
			# recursive case
			for i in [1..Length(path)] do
				e:=path[i];
				len := Length(path);
				
				# if l>1 then we are at a crossing
				if len > 1 then
					v := Intersection(VerticesOfEdge(surf,e[1]),VerticesOfEdge(surf,path[2][1]))[1];
					branches := [];
					
					for j in [1..len] do
						branch := Flat(ShallowCopy(path[j]))[1];
						branches[j]:= [branch,path[j]];
					od;
					info := [v,branches];
					data_fix := FixRamIntersection(surf,[e,is_circle,inner],info,data,points,Coords,shift_param);
					points := data_fix[1];
					Coords := data_fix[2];
				else
					if not IsInt(e) then
						e := e[1];
					fi;
					if e in Edges(surf) then
						if inner[e] or is_circle then
							data_fix:=FixVertOfInnerRamEdge(surf,e,Coords,points,data,shift_param,not_split);
							Coords:=data_fix[1];
							points:=data_fix[2];
							not_split := [];
						elif Length(path) = 1 then
							verts_current := VerticesOfEdge(surf,e);
							
							same := ChooseIsolatedVertex(surf,e,points,data);
							data_fix := FixVertOfOuterRamEdge(surf,e,Coords,points,data,shift_param,not_split);
				      			Coords:=data_fix[1];
							points:=data_fix[2];
						else
						    	verts_current := VerticesOfEdge(surf,e);
						    	if i = 1 then
						     		verts_comp :=  VerticesOfEdge(surf,Flat(ShallowCopy(path))[i+1]);
						  	else
						   		verts_comp :=  VerticesOfEdge(surf,Flat(ShallowCopy(path))[i-1]);
							fi;
						 	# vertex which does not need to be split
						 	same := Filtered(verts_current,x-> not x in verts_comp)[1];
							data_fix := FixVertOfOuterRamEdge(surf,e,Coords,points,data,shift_param,not_split);
				      			Coords:=data_fix[1];
							points:=data_fix[2];
							not_split := data_fix[3];
						fi;
					else
						Print("mistake somewhere?");
						#data_fix := FixRamPathRec(surf,[e,is_circle,inner],data,points,Coords,shift_param);
						#points := data_fix[1];
						#Coords := data_fix[2];
					fi;
					
				fi;
		    	od;
	    	fi;
	    	
	od;
	points_fix := points;
    	return [points_fix,Coords];
end;;

FixRamPathOuter := function(surf,order,data,points,Coords,shift_param)
	local l, comp, path, not_split, data_fix, inner, is_circle, ram_verts, points_fix, s_data, verts_current, verts_comp, same, e, s,t;
	not_split := [];
	is_circle := order[2];
	inner := order[3];
	
	for comp in order[1] do
		path := comp;
		data_fix := FixRamPathRec(surf,[comp,is_circle,inner],data,points,Coords,shift_param);
		points := data_fix[1];
		Coords:= data_fix[2];
		
	od;
	
	s_data := SimplicialSurfaceFromChangedCoordinates([Coords,surf],1./10^6);
	surf := s_data[1];
	points_fix := points;
	
	# fix ramified vertices
	
	ram_verts := RamifiedVertices(surf);
    	return [surf,points_fix,Coords];
end;;



FixRamVerts := function(surf, data, points, Coords, shift_param)
	local ram_verts, v, new_data, s_data;
	
	# to be executed after unramifying the edges
	
	ram_verts := ShallowCopy(RamifiedVertices(surf));
	
	for v in ram_verts do
		new_data := FixRamVert(surf,v,Coords,points,data,shift_param);
		Coords := new_data[1];
		points := new_data[2];
		
	od;
	
	s_data := SimplicialSurfaceFromChangedCoordinates([Coords,surf],1./10^6);
	surf := s_data[1];
	
	Coords := FixNormals(Coords);
	# TODO: check if normals are correct
	return [surf,points,Coords];
end;;


##############################################################################################

# does not work bc of loops - pathological?
test_surf_pts := [
[1.0,0.,0.],
[0.,1.0,0.],
[-1.0,0.,0.],
[0.,-1.0,0.],
[0.,0.,0.],
[0.,0.,1.0],
];

t_s := TriangularComplexByVerticesInFaces([[1,2,5],[1,4,5],[3,4,5],[2,3,5],[1,5,6],[4,5,6],[3,5,6],[2,5,6],[1,2,6],[1,4,6],[3,4,6],[2,3,6]]);


t_datas:=last;
t_points:=test_surf_pts;
t_points_fix := ShallowCopy(t_points);
t_ss:=t_s;

Coords := ConvertDataFormatPtC(t_ss,t_points_fix,Normals(t_ss,test_surf_pts));
datas:=[t_s,t_ss,Faces(t_ss),Normals(t_ss,test_surf_pts)];

t_ss := ShallowCopy(datas[2]);
shift_param:=0.04;

order_data := OrderRamifiedEdges(t_ss,ShallowCopy(datas));


unramified_data := FixRamPathRec(t_ss,order_data,datas,t_points_fix,Coords,shift_param);

unram_surf := unramified_data[1];
unram_points := unramified_data[2];

DrawSTLwithNormals(unram_surf,"test_surf_path_repaired_mew",unram_points,datas[4],[]);


##############################################################################################


Coord3_1:= [
                        [  0.5877852523,  0.0000000000,  0.0000000000 ],
                        [ -0.2628655561,  0.5257311121,  0.0000000000 ],
                        [ -0.2628655561, -0.4253254041,  0.3090169943 ],
                        [  0.4979796570,  0.8057480107,  0.5854101964 ],
                        [ -0.2628655561,  0.1624598481,  0.4999999999 ],
                        [ -0.2628655561,  0.1624598481, -0.4999999999 ],
                        [  0.2628655561, -0.1624598481, -0.4999999999 ],
                        [  0.2628655561, -0.1624598481,  0.4999999999 ],
                        [  0.2628655561,  0.4253254041, -0.3090169943 ],
                        [  0.2628655561, -0.5257311121,  0.0000000000 ],
                        [ -0.4979796570, -0.8057480107, -0.5854101964 ],
                        [ -0.5877852523,  0.0000000000,  0.0000000000 ],
                        ];
                        
calculate_intersections(VerticesOfFaces(Icosahedron()),Coord3_1,false,Group(()));
datas:=last;
points:=datas[1];
points_fix := ShallowCopy(points);
t:=TriangularComplexByVerticesInFaces(datas[2]); 
VerticesOfFace(t,1); 
n:=Crossproduct(points[2]-points[1],points[3]-points[1]);
n:=-n/Norm2(n);
datas:=OuterHull(t,points,1,-n);

t := ShallowCopy(datas[2]);
shift_param:=0.04;

order_data := OrderRamifiedEdges(t,ShallowCopy(datas));

Coords := ConvertDataFormatPtC(t,points_fix,datas[4]);
    	
unramified_data := FixRamPathOuter(t,order_data,datas,points_fix,Coords,shift_param);

unram_surf := unramified_data[1];
unram_points := unramified_data[2];
unram_coords := unramified_data[3];


fully_unramified_data := FixRamVerts(unram_surf,datas,ShallowCopy(unram_points),unram_coords,shift_param);

fully_unram_surf := fully_unramified_data[1];
fully_unram_points := fully_unramified_data[2];
fully_unram_coords := fully_unramified_data[3];



DrawSTLwithNormals(fully_unram_surf,"ico_3_1_path_repaired",fully_unram_points,datas[4],[]);


############################################################################################

Coord3_2:= [
[  0.9510565160,  0.0000000000,  0.0000000000 ],
[  0.4253254040,  0.8506508085,  0.0000000000 ],
[  0.4253254040,  0.2628655560,  0.8090169940 ],
[ -0.0449027976, -0.0277514551,  0.0854101965 ],
[  0.4253254040, -0.6881909604, -0.4999999998 ],
[  0.4253254040, -0.6881909604,  0.4999999998 ],
[ -0.4253254040,  0.6881909604,  0.4999999998 ],
[ -0.4253254040,  0.6881909604, -0.4999999998 ],
[ -0.4253254040, -0.2628655560, -0.8090169940 ],
[ -0.4253254040, -0.8506508085,  0.0000000000 ],
[  0.0449027976,  0.0277514551, -0.0854101965 ],
[ -0.9510565160,  0.0000000000,  0.0000000000 ],
];
calculate_intersections(VerticesOfFaces(Icosahedron()),Coord3_2,false,Group(()));
data:=last; 
points:=data[1];
points_fix := ShallowCopy(points);
t:=TriangularComplexByVerticesInFaces(data[2]); 
VerticesOfFace(t,1); 
n:=Crossproduct(points[2]-points[1],points[3]-points[1]);
n:=-n/Norm2(n);
data:=OuterHull(t,points,1,-n);

t := ShallowCopy(data[2]);
shift_param:=0.04;

order_data := OrderRamifiedEdges(t,ShallowCopy(data));
Coords := ConvertDataFormatPtC(t,points_fix,data[4]);

unramified_data := FixRamPathOuter(t,order_data,data,points_fix,Coords,shift_param);

unram_surf := unramified_data[1];
unram_points := unramified_data[2];
unram_coords := unramified_data[3];


fully_unramified_data := FixRamVerts(unram_surf,data,ShallowCopy(unram_points),unram_coords,shift_param);

fully_unram_surf := fully_unramified_data[1];
fully_unram_points := fully_unramified_data[2];
fully_unram_coords := fully_unramified_data[3];



DrawSTLwithNormals(fully_unram_surf,"ico_3_2_path_repaired",fully_unram_points,data[4],[]);


############################################## TEST 1_1

Coord1_1:= [
                        [  0.0000000000, -0.0564956995,  0.4967979831 ],
                        [  0.0000000000,  0.0564956995, -0.4967979831 ],
                        [ -0.8660254035,  0.0000000000,  0.0000000000 ],
                        [  0.8660254035,  0.0000000000,  0.0000000000 ],
                        [  0.2379137858,  0.7781232521,  0.0000000000 ],
                        [ -0.5739569990,  0.7577886993,  0.5834829341 ],
                        [ -0.2379137858, -0.7781232521,  0.0000000000 ],
                        [  0.5739569990, -0.7577886993, -0.5834829341 ],
                        [  0.2886751345,  0.1765267750, -0.7971856525 ],
                        [ -0.4688236925,  0.0868487119, -0.1505378568 ],
                        [ -0.2886751345, -0.1765267750,  0.7971856525 ],
                        [  0.4688236925, -0.0868487119,  0.1505378568 ],
                        ];

calculate_intersections(VerticesOfFaces(Icosahedron()),Coord1_1,false,Group(()));
data:=last;
points:=data[1];
points_fix := ShallowCopy(points);
t:=TriangularComplexByVerticesInFaces(data[2]);
VerticesOfFace(t,1); 
n:=Crossproduct(points[2]-points[1],points[3]-points[1]);
n:=-n/Norm2(n);
data:=OuterHull(t,points,1,n);

t := ShallowCopy(data[2]);
shift_param:=0.04;

order_data := OrderRamifiedEdges(t,ShallowCopy(data));
Coords := ConvertDataFormatPtC(t,points_fix,data[4]);

unramified_data :=FixRamPathOuter(t,order_data,data,points_fix,Coords,shift_param);

unram_surf := unramified_data[1];
unram_points := unramified_data[2];
unram_coords := unramified_data[3];


fully_unramified_data := FixRamVerts(unram_surf,data,ShallowCopy(unram_points),unram_coords,shift_param);

fully_unram_surf := fully_unramified_data[1];
fully_unram_points := fully_unramified_data[2];
fully_unram_coords := fully_unramified_data[3];


DrawSTLwithNormals(fully_unram_surf,"ico_1_1_path_repaired",fully_unram_points,data[4],[]);


################################################


Coord4_2:= 1.*[[-(17*Sqrt(5.))/60 - 1/12, 1/2, 0], [-(17*Sqrt(5.))/60 - 1/12, -1/2, 0], [-Sqrt(5.)/30 - 1/3, 0, Sqrt(5.)/4 + 1/4], [-Sqrt(5.)/30 - 1/3, 0, -Sqrt(5.)/4 - 1/4], [-Sqrt(5.)/30 + 1/6, Sqrt(5.)/4 + 1/4, -1/2], [-Sqrt(5.)/30 + 1/6, Sqrt(5.)/4 + 1/4, 1/2], [-Sqrt(5.)/30 + 1/6, -Sqrt(5.)/4 - 1/4, 1/2], [-Sqrt(5.)/30 + 1/6, -Sqrt(5.)/4 - 1/4, -1/2], [Sqrt(5.)/6 - 1/3, 0, (3*Sqrt(5.))/20 - 1/4], [(13*Sqrt(5.))/60 + 5/12, 1/2, 0], [Sqrt(5.)/6 - 1/3, 0, -(3*Sqrt(5.))/20 + 1/4], [(13*Sqrt(5.))/60 + 5/12, -1/2, 0]];

calculate_intersections(VerticesOfFaces(Icosahedron()),Coord4_2,false,Group(()));
data:=last; 
points:=data[1];
points_fix := ShallowCopy(points);
t:=TriangularComplexByVerticesInFaces(data[2]); 
VerticesOfFace(t,1); 
n:=Crossproduct(points[2]-points[1],points[3]-points[1]);
n:=-n/Norm2(n);
data:=OuterHull(t,points,1,-n);
t 
shift_param:=0.04;

order_data := OrderRamifiedEdges(t,ShallowCopy(data));

unramified_data := FixRamPath(t,order_data,data,points_fix,shift_param);

unram_surf := unramified_data[1];
unram_points := unramified_data[2];
unram_coords := unramified_data[3];

fully_unramified_data := FixRamVerts(unram_surf,data,ShallowCopy(unram_points),unram_coords,shift_param);

fully_unram_surf := fully_unramified_data[1];
fully_unram_points := fully_unramified_data[2];
fully_unram_coords := fully_unramified_data[3];


DrawSTLwithNormals(fully_unram_surf,"ico_4_2_path_repaired",fully_unram_points,data[4],[]);


################################################

Coord4_1:= 1.*[[(17*Sqrt(5.))/60 - 1/12, 1/2, 0], [(17*Sqrt(5.))/60 - 1/12, -1/2, 0], [Sqrt(5.)/30 - 1/3, 0, -Sqrt(5.)/4 + 1/4], [Sqrt(5.)/30 - 1/3, 0, Sqrt(5.)/4 - 1/4], [Sqrt(5.)/30 + 1/6, -Sqrt(5.)/4 + 1/4, -1/2], [Sqrt(5.)/30 + 1/6, -Sqrt(5.)/4 + 1/4, 1/2], [Sqrt(5.)/30 + 1/6, Sqrt(5.)/4 - 1/4, 1/2], [Sqrt(5.)/30 + 1/6, Sqrt(5.)/4 - 1/4, -1/2], [-Sqrt(5.)/6 - 1/3, 0, -(3*Sqrt(5.))/20 - 1/4], [-(13*Sqrt(5.))/60 + 5/12, 1/2, 0], [-Sqrt(5.)/6 - 1/3, 0, (3*Sqrt(5.))/20 + 1/4], [-(13*Sqrt(5.))/60 + 5/12, -1/2, 0]];

calculate_intersections(VerticesOfFaces(Icosahedron()),Coord4_1,false,Group(()));
data:=last; 
points:=data[1];
points_fix := ShallowCopy(points);
t:=TriangularComplexByVerticesInFaces(data[2]); 
VerticesOfFace(t,1); 
n:=Crossproduct(points[2]-points[1],points[3]-points[1]);
n:=-n/Norm2(n);
data:=OuterHull(t,points,1,-n);

t := ShallowCopy(data[2]);
shift_param:=0.04;

order_data := OrderRamifiedEdges(t,ShallowCopy(data));

unramified_data := FixRamPath(t,order_data,data,points_fix,shift_param);

unram_surf := unramified_data[1];
unram_points := unramified_data[2];
unram_coords := unramified_data[3];

fully_unramified_data := FixRamVerts(unram_surf,data,ShallowCopy(unram_points),unram_coords,shift_param);

fully_unram_surf := fully_unramified_data[1];
fully_unram_points := fully_unramified_data[2];
fully_unram_coords := fully_unramified_data[3];


DrawSTLwithNormals(fully_unram_surf,"ico_4_1_path_repaired",fully_unram_points,data[4],[]);

################################################################################
# problematic surfaces: ico_9_2 TODO: doesnt work

Coord9_2 := [[  0.5044801387,  0.0000000000,  0.0000000000 ],
             [ -0.4866391575,  0.132975715,  0.0000000000 ],
	     [ -0.1062135603, -0.7916506344,  0.0185061861 ],
	     [ -0.1062135603, -0.7916506344, -0.0185061861 ],
	     [ -0.4849729828,  0.1330303242, -0.0573188722 ],
	     [ -0.0700466033, -0.1696734905,  0.8007058944 ],
	     [  0.5028872829,  0.0004918641,  0.0573188722 ],
	     [  0.022845233, -0.1821365294, -0.8007058944 ],
	     [  0.499298766,  0.0003685204,  0.0593429052 ],
	     [  0.0440498825,  0.7760568202,  0.4964425686 ],
	     [ -0.4815438862,  0.1319654467, -0.0593429052 ],
	     [  0.1620684474,  0.7602225981, -0.4964425686 ]];

calculate_intersections(VerticesOfFaces(Icosahedron()),Coord9_2,false,Group(()));
data:=last; 
points:=data[1];
points_fix := ShallowCopy(points);
t:=TriangularComplexByVerticesInFaces(data[2]); 
VerticesOfFace(t,1); 
n:=Crossproduct(points[2]-points[1],points[3]-points[1]);
n:=-n/Norm2(n);
data:=OuterHull(t,points,1,-n);

t := ShallowCopy(data[2]);
shift_param:=0.04;

order_data := OrderRamifiedEdges(t,ShallowCopy(data));

unramified_data := FixRamPath(t,order_data,data,points_fix,shift_param);

unram_surf := unramified_data[1];
unram_points := unramified_data[2];

DrawSTLwithNormals(unram_surf,"ico_9_2_path_repaired",unram_points,data[4],[]);


################################################################################
# problematic surfaces: ico_13_4 TODO: doesnt work

Coord13_4:= [[  0.5943546867,  0.0000000000,  0.0000000000 ],
	     [ -0.2275545177,  0.5696185211,  0.0000000000 ],
             [ -0.2255578631, -0.3052809294,  0.4843004902 ],
			[  0.0064226535,  0.0294464622, -0.8083741894 ],
			[ -0.0515757990, -0.7428783772, -0.1757996706 ],
			[ -0.3111407222,  0.2025167206, -0.3729142558 ],
			[  0.4813343426,  0.3750174626,  0.6779432216 ],
			[  0.1219744829, -0.3614287659,  0.1047875327 ],
			[ -0.7816873474, -0.0599569290, -0.1993670794 ],
			[ -0.1439733572, -0.0837561139,  0.5705384119 ],
			[  0.5625521379, -0.215877538, -0.1247066198 ],
			[ -0.0251486969,  0.592579487, -0.1564078413 ],
			];

calculate_intersections(VerticesOfFaces(Icosahedron()),Coord13_4,false,Group(()));
data:=last; 
points:=data[1];
points_fix := ShallowCopy(points);
t:=TriangularComplexByVerticesInFaces(data[2]); 
VerticesOfFace(t,1); 
n:=Crossproduct(points[2]-points[1],points[3]-points[1]);
n:=-n/Norm2(n);
data:=OuterHull(t,points,1,-n);

t := ShallowCopy(data[2]);
shift_param:=0.01;

order_data := OrderRamifiedEdges(t,ShallowCopy(data));

unramified_data := FixRamPath(t,order_data,data,points_fix,shift_param);

unram_surf := unramified_data[1];
unram_points := unramified_data[2];

DrawSTLwithNormals(unram_surf,"ico_13_4_path_repaired",unram_points,data[4],[]);


################################################################################
# double 6-Gon

points:=[[0,0,0],
		[1,0,0],
		[1.5,-Sqrt(3.)/2,0],
		[1,-Sqrt(3.),0],
		[0,-Sqrt(3.),0],
		[-0.5,-Sqrt(3.)/2,0],
		[0.5,-Sqrt(3.)/2,1],
		[0.5,-Sqrt(3.)/2,-1]];

faces:=[[1,2,7],[2,3,7],[3,4,7],[4,5,7],[5,6,7],[6,1,7],  [1,2,8],[2,3,8],[3,4,8],[4,5,8],[5,6,8],[6,1,8]
 ];

# join 4-copies to create Ramified Edges

translations:=[[1.5,Sqrt(3.)/2,0],[1.5,-Sqrt(3.)/2,0],[3,0,0],[0,0,0]];

new_faces:=[];
new_points:=[];
for t in [1..Size(translations)] do
	new_faces:=Concatenation(new_faces,faces+Size(points)*(t-1));
	new_points:=Concatenation(new_points,points+translations[t]);
od;
t:=TriangularComplexByVerticesInFaces(new_faces);
data:=clean_data([new_points,VerticesOfEdges(t)],eps);
data:=triangulate(data);
points:=data[1];
t:=TriangularComplexByVerticesInFaces(data[2]);

calculate_intersections(VerticesOfFaces(t),points,false,Group(()));
data:=last; 
points:=data[1];
points_fix := ShallowCopy(points);
t:=TriangularComplexByVerticesInFaces(data[2]); 
VerticesOfFace(t,1); 
n:=Crossproduct(points[2]-points[1],points[3]-points[1]);
n:=-n/Norm2(n);
data:=OuterHull(t,points,1,n);
t := ShallowCopy(data[2]);
shift_param:=0.04;

order_data := OrderRamifiedEdges(t,ShallowCopy(data));

unramified_data := FixRamPath(t,order_data,data,points_fix,shift_param);

unram_surf := unramified_data[1];
unram_points := unramified_data[2];

DrawSTLwithNormals(unram_surf,"double_6_gon_merged",unram_points,data[4],[]);





