Read("my_triangle_fix.g");
Read("my_triangle_fix.g");
### Example for triangulation function (by Tom)

### Icosahedron 2_1

# 2_1 Icosahedron coordinates

phi:=(1+Sqrt(5.))/2;

coordinates:=1./(2*phi)*[
	[0, 1, phi],
	[0, 1, -phi],
	[-1, -phi, 0],
	
	
	[1, -phi, 0],
	[-phi, 0, -1],
	[phi, 0, -1],
	[phi, 0, 1],
	
	
	[-phi, 0, 1],
	[1, phi, 0],
	[0, -1, phi],
	[-1, phi, 0],
	
	
	[0, -1, -phi]
	];

# we set epsilon

eps:=0.0001;

# here we find all self-intersections and retriangulate
# if we set the last parameter to true we also look for the self-intersections (this is much faster)
# the following command takes about 98008 milliseconds
data:=calculate_intersections(VerticesOfFaces(Icosahedron()),coordinates,false);

# in data we have the vertices and the vertices of faces
points:=data[1];;
t:=TriangularComplexByVerticesInFaces(data[2]);

# no we want to calculate the outer hull

# normal vector for the first face
n:=Crossproduct(points[8]-points[1],points[15]-points[1]);
n:=-n/Norm2(n);
data:=OuterHull(t,points,1,n); 

# now we can create an STL file

DrawSTLwithNormals(data[2],"ico_2_1",points,data[4],[]);     

# we can check the Automorphism Group of the outer hull which is indeed the full automorphism group of the icosahedron as it should be :)

StructureDescription(AutomorphismGroup(data[2]));

# Here is another example

Coord6_1:= [
[  0.5405163169,  0.0000000000,  0.0000000000 ],
[ -0.3845251375,  0.3798661705,  0.0000000000 ],
[ -0.1096579179, -0.2670364664,  0.7113121607 ],
[ -0.1096579179, -0.2670364664, -0.7113121607 ],
[ -0.1096579179,  0.6691652569, -0.3598490285 ],
[ -0.3845251375, -0.2860192200, -0.2499826258 ],
[  0.5482895894,  0.3989806086,  0.3598490285 ],
[  0.0725427786, -0.4737131210,  0.2499826258 ],
[ -0.7676054254,  0.0031481819, -0.0083858962 ],
[  0.0725427786,  0.1921722697,  0.4999652517 ],
[  0.5482895894, -0.5372211148,  0.0083858962 ],
[  0.0834484007,  0.1876939007, -0.4999652517 ],
];

# the following takes 1427 milliseconds
data:=calculate_intersections(VerticesOfFaces(Icosahedron()),Coord6_1,false);


# in data we have the vertices and the vertices of faces
points:=data[1];
t:=TriangularComplexByVerticesInFaces(data[2]);

# no we want to calculate the outer hull

# normal vector for the first face
n:=Crossproduct(points[15]-points[53],points[53]-points[22]);
n:=n/Norm2(n);
data:=OuterHull(t,points,98,n); 

# the automorphism group is correct
StructureDescription(AutomorphismGroup(data[2]));

# now we generate an STL file
DrawSTLwithNormals(data[2],"ico_6_1",points,data[4],[]);

# let's take a look at the outer hull
t:=data[2];
# it is not a simplicial surface since there are ramified edges and ramified vertices
# this can also be visualized by the stl file
RamifiedVertices(t);
RamifiedEdges(t);

# the function was tested for all icosahedra in the table from 1-6
# the first problem for now appears with 7.1:

Coord7_1:= [
[  0.5009967201,  0.0000000000,  0.0000000000 ],
[ -0.4970138058,  0.0630475245,  0.0000000000 ],
[ -0.0526004790, -0.8326390625,  0.0155670400 ],
[ -0.0526004790, -0.8326390625, -0.0155670400 ],
[ -0.3648908737, -0.0154491110, -0.4999999999 ],
[ -0.3648908737, -0.0154491110,  0.4999999999 ],
[  0.3600458175, -0.0612456860,  0.4999999999 ],
[  0.3600458175, -0.0612456860, -0.4999999999 ],
[  0.0039614511,  0.0627077740,  0.4261962339 ],
[ -0.4475126360,  0.8466260854,  0.0000000000 ],
[  0.0039614511,  0.0627077740, -0.4261962339 ],
[  0.5504978899,  0.7835785607,  0.0000000000 ],
];

Coord7_1_alt:=TransposedMat([[-0.8167271808002584247083256859436, -0.8167271808002584247083256859436, -0.062832778270196341340310703447, -0.062832778270196341340310703447, 0.03842384176228597498511633075192, 0.03842384176228597498511633075192, 0.03842384176228597498511633075192, 0.03842384176228597498511633075192, 0.83429887857800569647149116978269, -0.03158660303212288039308744158, 0.83429887857800569647149116978269, -0.03158660303212288039308744158], [0.50000000000000000000000000000000, -0.50000000000000000000000000000000, 0., 0., 0.36319090450472717547834898233, 0.36319090450472717547834898233, -0.36319090450472717547834898233, -0.36319090450472717547834898233, 0., -0.50000000000000000000000000000000, 0., 0.50000000000000000000000000000000], [0., 0., 0.42619623395079492535990527253998, -0.42619623395079492535990527253998, 0.50000000000000000000000000000000, -0.50000000000000000000000000000000, -0.50000000000000000000000000000000, 0.50000000000000000000000000000000, 0.0155670400781807627258825579691, 0., -0.0155670400781807627258825579691, 0.]]);

# how do we choose eps?

# we use a different eps for detecting intersections and fixing intersections
eps:=1.*10^-8;

# the following command takes 7358 ms
l:=calculate_intersections(VerticesOfFaces(Icosahedron()),Coord7_1,true);;

# now fix the intersections
data:=List(l,i->triangulate(my_triangle_fix(i)));;

# and choose different eps for joining triangles
eps:=1.*10^-6;
data:=join_triangles(data);

# in data we have the vertices and the vertices of faces
points:=data[1];
t:=TriangularComplexByVerticesInFaces(data[2]);

# we see that for eps:=0.001 the aut. group is very large which looks promising for eps:=1.*10^-6; its a bit too small
StructureDescription(AutomorphismGroup(t));

# no we want to calculate the outer hull

# normal vector for the first face
VerticesOfFace(t,1);
n:=Crossproduct(points[94]-points[89],points[99]-points[89]);
n:=n/Norm2(n);
data:=OuterHull(t,points,433,-n); 

# now we can create an STL file

DrawSTLwithNormals(data[2],"ico_7_1",points,data[4],[]); 

# let's take a look at the outer hull
t:=data[2];
# it is not a simplicial surface since there are ramified edges
# this can also be visualized by the stl file
RamifiedVertices(t);
RamifiedEdges(t);

# the aut group of t is not C2 x C2 which shows that something went wrong
StructureDescription(AutomorphismGroup(t));