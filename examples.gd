#! @BeginGroup DrawChosenSurface
#! @Description
#! This method triangulates chosen example surfaces 
#! @Arguments surf_string, printSteps, Steps
DeclareOperation( "DrawChosenSurface",            [IsString,IsInt,IsList]);
#! @EndGroup

#! @BeginGroup Miscanelous test methods
#! @Description
#! Multiple small test, draw or triangulate methods for examples
DeclareOperation( "CalculateNumberOfIntersectionsAndRamifications", []);
DeclareOperation("TestEasy", []);
DeclareOperation("TestIco_21_32", []);
DeclareOperation("Test_NumbIntersections_Ico_21_32", []);
DeclareOperation("TestIco_11_31", []);
DeclareOperation("Test_NumbIntersections_Ico_11_31", []);
DeclareOperation("TestIco_42_41", []);
DeclareOperation("TestIco_52_51", []);
DeclareOperation("TestIco_62_61", []);
DeclareOperation("TestIco_72_71", []);
DeclareOperation("TestIco_73_81", []);
DeclareOperation("TestIco_92_91", []);
DeclareOperation("TestIco_94_93", []);
DeclareOperation("TestIco_95_141", []);
DeclareOperation("TestIco_102_101", []);
DeclareOperation("TestIco_112_111", []);
DeclareOperation("TestIco_122_121", []);
DeclareOperation("TestIco_124_123", []);
DeclareOperation("TestIco_132_131", []);
DeclareOperation("TestIco_134_133", []);
DeclareOperation("TestIco_136_135", []);
DeclareOperation("TestIntersections", []);
DeclareOperation("TestIntersections2", []);
DeclareOperation("TestTriang2", []);
DeclareOperation("TestTriang4", []);
DeclareOperation("TestOcta1", []);
DeclareOperation("TestOcta2", []);
DeclareOperation("TriangulateShells", []);
DeclareOperation("TriangulateSurface", []);
DeclareOperation("TriangulateSurface2", []);
#! @EndGroup
