#3_2

ico := Icosahedron();
        
Coord3_2:= [
                    [  0.9510565160,  0.0000000000,  0.0000000000 ],
                    [  0.4253254040,  0.8506508085,  0.0000000000 ],
                    [  0.4253254040,  0.2628655560,  0.8090169940 ],
                    [ -0.0449027976, -0.0277514551,  0.0854101965 ],
                    [  0.4253254040, -0.6881909604, -0.4999999998 ],
                    [  0.4253254040, -0.6881909604,  0.4999999998 ],
                    [ -0.4253254040,  0.6881909604,  0.4999999998 ],
                    [ -0.4253254040,  0.6881909604, -0.4999999998 ],
                    [ -0.4253254040, -0.2628655560, -0.8090169940 ],
                    [ -0.4253254040, -0.8506508085,  0.0000000000 ],
                    [  0.0449027976,  0.0277514551, -0.0854101965 ],
                    [ -0.9510565160,  0.0000000000,  0.0000000000 ],
                    ];
pRecord1 := SetVertexCoordinates3D(ico, Coord3_2, rec());
DrawComplexToJavaScript(ico,"ico_32",pRecord1);

# intersection example 1
coordinates:=[[0.,0.,1.],[1.5,0.,1],[0.,2.,1.],[0.75,0.5,1.5],[0.75,0.5,0.5],[2,0.5,0.5]];

faces:=[ [ 1, 2, 3 ], [ 4, 5, 6 ]];

t:=TriangularComplexByVerticesInFaces(faces);
pRecord2 := SetVertexCoordinates3D(t, coordinates, rec());



DrawComplexToJavaScript(t,"Intersection",pRecord2);

# intersection example 2
coordinates:=[[0.,0.,1.],[1.,0.,1],[0.,2.,1.],[1.5,0,1],[0.5,1.,1]];

faces:=[ [ 1, 2, 3 ], [ 2, 4, 5]];

t:=TriangularComplexByVerticesInFaces(faces);
pRecord2 := SetVertexCoordinates3D(t, coordinates, rec());


DrawComplexToJavaScript(t,"Intersection2",pRecord2);


# ramified example

coordinates:=[[0.,-1.,0.],[0.,1.,0.],[0.5,1.,0.5],[0.5,1.,-0.5],[-0.5,1.,0.5],[-0.5,1.,-0.5]];

faces:=[ [ 1, 2, 3 ], [ 1, 2, 4], [ 1, 2, 5], [ 1, 2, 6]];

t:=TriangularComplexByVerticesInFaces(faces);
pRecord3 := SetVertexCoordinates3D(t, coordinates, rec());
SetEdgeColour(t,1,0xFF0000,pRecord3);
pRecord3:=ActivateLineWidth(t, pRecord3);


DrawComplexToJavaScript(t,"ramified1",pRecord3);

# unramified example

coordinates:=[[0.,-1.,-0.1],[0.,1.,-0.1],[0.,-1.,0.1],[0.,1.,0.1],[0.5,1.,0.5],[0.5,1.,-0.5],[-0.5,1.,0.5],[-0.5,1.,-0.5]];

faces:=[ [ 3, 4, 5 ], [ 1, 2, 6], [ 3, 4, 7], [ 1, 2, 8]];

t:=TriangularComplexByVerticesInFaces(faces);
pRecord3 := SetVertexCoordinates3D(t, coordinates, rec());
SetEdgeColour(t,1,"0xFF0000",pRecord3);
SetEdgeColour(t,6,"0xFF0000",pRecord3);
pRecord3:=ActivateLineWidth(t, pRecord3);


DrawComplexToJavaScript(t,"un_ramified1",pRecord3);
