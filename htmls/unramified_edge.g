LoadPackage("gapic"); 
coordinates:=[[0.,0.,0.02],[-0.5,0.,0.5],[0.5,0.,0.5],[0.,2.,0.02],[0.,0.,0.],[-0.5,0.,-0.5],[0.5,0.,-0.5],[0.,2.,0.]];

new_faces:=[ [ 1, 2, 4 ], [ 1, 3, 4 ], [ 5, 6, 8 ], [ 5, 7, 8 ]];
t:=TriangularComplexByVerticesInFaces(new_faces);

pr:=rec( drawEdges := [ false, false, true, false, false, false, false, true, false, false], edgeThickness := 1, edgeColours := [ ,,"0xFF0000",,,,,"0xFF0000",, ],  vertexCoordinates3D := coordinates );
pr:=ActivateLineWidth(t, pr);

DrawComplexToJavaScript(t,"ramified_edge_fixed",pr);

----------------------------------------------------------------------------------


coordinates:=[[0.,0.,0.0],[1.,0.,0.],[0.5,0.5,0.],[-0.5,1.5,0.0],[0.5,1.75,0.0]];

new_faces:=[ [ 1, 2, 3 ], [ 3, 4, 5 ]];
t:=TriangularComplexByVerticesInFaces(new_faces);

pr:=rec( edgeThickness := 1, vertexCoordinates3D := coordinates );


DrawComplexToJavaScript(t,"no_intersection",pr);

-----------------------------------------------------------------------------------

LoadPackage("gapic"); 
coordinates:=[[0.,0.,0.0],[1.,0.,0.],[0.5,0.5,0.],[0.35,0.25,0.25],[0.35,0.25,-0.25],[0.,0.25,0.]];

new_faces:=[ [ 1, 2, 3 ], [ 4, 5, 6 ]];
t:=TriangularComplexByVerticesInFaces(new_faces);

pr:=rec( edgeThickness := 1, vertexCoordinates3D := coordinates );

DrawComplexToJavaScript(t,"intersection_type3",pr);
