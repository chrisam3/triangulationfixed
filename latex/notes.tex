\documentclass[11pt,a4paper]{article}


\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[ngerman]{babel}

\usepackage{lmodern}


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%	math fonts

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{amstext} 
\usepackage{mathtools}

\usepackage{mathrsfs}
\usepackage{verbatim}
\usepackage{geometry}

\newtheorem{thm}{Theorem}
\newtheorem{lem}{Lemma}
\newtheorem{rem}{Remark}
%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%	misc packages

\usepackage{graphicx}
\usepackage{letltxmacro}
\usepackage{subcaption}
\usepackage{float}	
\usepackage{parskip}

\usepackage{ifthen}
\usepackage{color}             
\usepackage{tikz}    
\usepackage{tikz-3dplot}          
\usepackage{hyperref,breakurl}  

\usepackage{stmaryrd}
\usepackage{todonotes}
\usepackage{paralist}
\usepackage{esint}

%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%	own commands

% scalar product
\newcommand{\lang}{\begin{picture}(5,7)
\put(1.1,2.5){\rotatebox{45}{\line(1,0){6.0}}}
\put(1.1,2.5){\rotatebox{315}{\line(1,0){6.0}}}
\end{picture}}
\newcommand{\rang}{\begin{picture}(5,7)
\put(.1,2.5){\rotatebox{135}{\line(1,0){6.0}}}
\put(.1,2.5){\rotatebox{225}{\line(1,0){6.0}}}
\end{picture}}

\newcommand{\scalp}[2]{\lang #1,#2\rang\ }

% nice square root
\makeatletter
\let\oldr@@t\r@@t
\def\r@@t#1#2{%
\setbox0=\hbox{$\oldr@@t#1{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.2\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\box0\lower0.4pt\box2}}
\LetLtxMacro{\oldsqrt}{\sqrt}
\renewcommand*{\sqrt}[2][\ ]{\oldsqrt[#1]{#2}}
\makeatother


%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Detection and Rectification of Self-Intersections in Simplicial Surfaces}
\author{Christian Amend}

\begin{document}
\maketitle

\section{Goal}
The main task is to be able to 3D-Print closed simplicial surfaces, which are geometric objects composed of triangles, like the tetrahedron \ref{simpl:tetra}.\\ For this, an $stl$ file of an embedding in $\mathbb{R}^3$ has to be created and possible self-intersections have to be detected and repaired. Then, the $stl$ file serves as an input for a slicing program like Prusa Slicer that translates the geometric information into instructions for the 3D-Printer. \\

\begin{figure} [H]
\centering

\tdplotsetmaincoords{65}{150}
\begin{tikzpicture}[tdplot_main_coords, line join = round, line cap = round]
\pgfmathsetmacro{\factor}{1/sqrt(2)};
\coordinate [label=below:A] (A) at (2,0,-2*\factor);
\coordinate [label=right:B] (B) at (-2,0,-2*\factor);
\coordinate [label=above:C] (C) at (0,2,2*\factor);
\coordinate [label=left:D] (D) at (0,-2,2*\factor);

\draw[-, fill=red!30, opacity=.7] (A)--(D)--(B)--cycle;
\draw[-, fill=green!30, opacity=.7] (A) --(D)--(C)--cycle;
\draw[-, fill=purple!30, opacity=.7] (B)--(D)--(C)--cycle;
\end{tikzpicture}
\caption{A tetrahedron}
\label{simpl:tetra}
\end{figure}


\section{Background}
In the context of this paper, a simplicial surface\footnote{Original definition in \cite{Brakhage}, note that it has been simplified and altered for application purpose} $\mathcal{S}$ will be described as a (finite) collection of faces $F$, edges $E$ and vertices $V$ with a (incidence) relation $<$ such that the following holds:

\begin{enumerate}
\item For each edge $e \in E$ there are exactly two incident vertices $v \in V$ with $v<e$.
\item For each face $f \in F$ there are exactly three incident edges $f \in F$ with $e<f$ and exactly three incident vertices $v \in V$ with $v<f$. Additionally, each of the vertices is incident to exactly two of the edges.
\item For each edge $e \in E$ there is exactly one face $f \in F$ with $e<f$ or exactly two faces $f_1,f_2 \in F$ with 
$e<f_1$ and $e<f_2$. For a closed simplicial surface, there always have to be two faces.
\item For each vertex $v\in V$ it holds that $\mathrm{deg}(v) := |\{f \in F : v<f\}| < \infty$
\end{enumerate}

Additionally, and more important in this context, a simplical surface may be embedded:
\\
A embedding of an simplicial surface $\mathcal{S}$ is a map $\phi:V \longrightarrow \mathbb{R}^3$ that assigns each vertex a vector in euclidean space and fulfills:
\begin{enumerate}
\item For two different vertices $v_1,v_2 \in V$ it follows that $\phi(v_1) \neq \phi(v_2)$.
\item For two different faces $f_1,f_2 \in F$ with vertices $v_1^i$ and $v_2^j$ there is no permutation $\sigma$ of their vertices such that $\phi(v_1^i) = \phi(v_2^{\sigma(i)})$
\end{enumerate}
In short, $\phi$ should be injective and no face should be mapped onto another. It is also important to note that a simplicial surface may have multiple possible embedding(see Table 2 of \cite{Icosahedra:online}).\\
Then, each embedding also designates the normal vector of each face via the cross-product: \\
For $f\in F$ with vertices $v_1,v_2,v_3$, $\phi$ an embedding, the normal vector $n_f$ of $f$ is 
$$n_f := (v_1 - v_2) \times (v_1-v_3)$$

Together with the embedding, the normal vectors are sufficient to create a 3D model of the simplicial surface, as seen in \cite{Writinga88:online}. However if an embedding has a self-intersection (i.e. a intersection like in Figure \ref{self-intersection}, between two of its own faces), the created model can not be printed since the slicing process fails.\\


\begin{figure}[H]


\begin{subfigure}{0.5\textwidth}
\tdplotsetmaincoords{70}{120}
\begin{tikzpicture}[tdplot_main_coords]

\draw[thick,->] (0,0,0) -- (4,0,0) node[anchor=north east]{$x$};
\draw[thick,->] (0,0,0) -- (0,3,0) node[anchor=north west]{$y$};
\draw[thick,->] (0,0,0) -- (0,0,3) node[anchor=south]{$z$};

\draw[thick,color=blue,-] (0,0,1)-- (2,0,1) node[anchor=south east]{};
\draw[thick,color=blue,-] (2,0,1)-- (0,2,1) node[anchor=south west]{};
\draw[thick,color=blue,-] (0,2,1)-- (0,0,1) node[anchor=south east]{};



\draw[thick,color=green,-] (1,0,2)-- (1,0.5,0.5) node[anchor=north east]{};
\draw[thick,color=green,-] (1,0.5,0.5)-- (1,1,2) node[anchor=south west]{};
\draw[thick,color=green,-] (1,1,2)-- (1,0,2) node[anchor=south west]{};

\draw [thick, draw=green, fill=green, opacity=1]
       (1,0,2) -- (1,0.5,0.5) -- (1,1,2) -- cycle;

\draw [thick, draw=white, fill=blue, opacity=0.6]
       (0,0,1) -- (2,0,1) -- (0,2,1) -- cycle;
       
\draw[thick,color=red,-] (1,0.333333,1)-- (1,0.666666,1) node[anchor=north west]{\small{Intersection}};

\end{tikzpicture}
\caption{Intersection of two faces}
\end{subfigure}
\begin{subfigure}{0.5\textwidth}
\centering
\tdplotsetmaincoords{0}{180}
\begin{tikzpicture}[tdplot_main_coords]

\draw[thick,->] (0,0,0) -- (4,0,0) node[anchor=north east]{x};
\draw[thick,->] (0,0,0) -- (0,3,0) node[anchor=north west]{y};

\draw[thick,color=blue,-] (0,0,1)-- (2,0,1) node[anchor=south east]{};
\draw[thick,color=blue,-] (2,0,1)-- (0,2,1) node[anchor=south west]{};
\draw[thick,color=blue,-] (0,2,1)-- (0,0,1) node[anchor=south east]{};



\draw[thick,color=green,-] (1,0,2)-- (1,0.5,0.5) node[anchor=north east]{};
\draw[thick,color=green,-] (1,0.5,0.5)-- (1,1,2) node[anchor=south west]{};
\draw[thick,color=green,-] (1,1,2)-- (1,0,2) node[anchor=south west]{};



\draw [thick, draw=white, fill=blue, opacity=0.5]
       (0,0,1) -- (2,0,1) -- (0,2,1) -- cycle;
\draw [thick, draw=white, fill=green, opacity=0.6]
       (1,0,2) -- (1,0.5,0.5) -- (1,1,2) -- cycle;
              
\draw[very thick,color=red,-] (1,0.33,1)-- (1,0.66,1) node[anchor=south east]{\small{Intersection}};

\end{tikzpicture}
\caption{Rotated Intersection}
\end{subfigure}
\caption{Intersection of Triangles}
\label{self-intersection}
\end{figure}

One cause of this is that the program needs a clear distinction between the  \textit{outside} and \textit{inside} of a model. For this, the normal vectors of the faces are used - on one side is the model, on the other the outside.
With self-intersections, it is possible that for example holes in the model exists with indeterminable position relative to the printable object.

\todo{diamond-icosader would be good example here}

\section{Method}
In the following, we assume a choice of an embedding of a simplicial surface $\mathcal{S}$ has already been made, and identify $\mathcal{S}$ with its embedding. \\
Since the surfaces encountered mostly have few faces, there is not the greatest need for top efficiency. Thus, we simply iterate over all faces and check for each face if it intersects with any other face. If so, after some geometric considerations, we fix the intersection by deleting the current and intersecting face and then inserting new faces.  
\subsection{Detection of Intersections}
A simple way to check if two triangles $f_1,f_2$ intersect is by making use of their (geometric) edges. This is observable in the \textit{easy} case (see figure \ref{self-intersection}), where one triangle is fully submerged in another. However, there are also other geometric constellations of intersections: \todo{graphic}
\\
\\
\subsubsection{Calculation of edge-plane intersection points}
Firstly, we consider for the three vertices $v^1_1,v^1_2,v^1_3 < f_1$ their connection lines (which geometrically represent the edges for $\alpha \in [0,1]$): $$l_{ij}(\alpha) := v^1_i + \alpha\cdot (v^1_i-v^1_j),\ \mathrm{for}\ (i,j) \in \{(1,2),(2,3),(3,1)\}.$$
To calculate their intersection point with the plane $P(f_2)$ spanned by $f_2$ (i.e. by its normal vector $n_{f_2}$ together with $v^2_1$) we can use its normal equation:
\begin{align}
\label{eq:normal}
x\in P(f_2) \iff \scalp{x-v^2_1}{n_{f_2}} = 0
\end{align}
Thus,
\begin{align}
\label{eq:intersection}
l_{ij}(\alpha)\in P(f_2) \iff \alpha  = \dfrac{\scalp{v^2_1-v^1_i}{n_{f_2}}}{\scalp{v^1_i-v^1_j}{n_{f_2}}}
\end{align}
(disregarding the case when $\scalp{v^1_i-v^1_j}{n_{f_2}} = 0$, which means the edge is perpendicular to $f_2$ and needs to be checked beforehand, see \ref{sec:perp}). \\
To then check if the intersection point is inside the edge, $\alpha \in [0,1]$ needs to hold, but since we have to keep numerical errors in mind, we only require $\alpha \in [-\varepsilon,1+\varepsilon]$.
\subsubsection{Checking if a vector lies inside a face}
Next, we need to check if the intersection lies inside the face itself and not only the plane.\\
Thus, we are given a vector $v_0$ that we know lies on the plane $P(f)$ and want to calculate if $v_0$ lies inside $f$. This can be solved in a robust way by using geometry: if the vector is inside the face, its scalar product with the normal $n_i$ of the plane spanned by $n_f$ and $v_i - v_j$ (again for $(i,j) \in \{(1,2),(2,3),(3,1)\}$) has to be \textbf{non-negative} if $n_i$ points towards the inside of the face.\todo{graphic} \\

While it would be complicated to infer the order of the given vertices of the face, it is obvious that e.g.
$\scalp{n_1}{v_3} > 0$ has to hold if $n_1$ points in the right direction. With this information, we can then ensure the correct orientation of the normal vectors and then simply check if $\scalp{n_i}{v_0} \geq 0$ for $i\in \{1,2,3\}$.
\subsubsection{Using intersection points to infer self-intersection}
By using the methods described above, we can calculate all intersection points $I_1$ of edges of $f_1$ with $f_2$. However, we also need the reverse: the intersection points $I_2$ of edges of $f_2$ with $f_1$, as seen in figure \todo{grapic}. \\
Then, we also need to account for numerical error and remove \textit{numeric} duplicates, which means identifying 
$$v = w \iff ||v-w||_2 \leq \varepsilon.$$
Lastly, we can examine $I_1 \cup I_2$, as its cardinality $c := |I_1 \cup I_2|$ tells us if an self-intersection is present: If $c = 2$, the faces intersect, but if $c \in \{0,1\}$, they do not(see figure \todo{graphic}). \\
The intersection is therefore parameterised by $v_1,v_2$ with $I_1 \cup I_2 = \{v_1,v_2\}$, which will be of importance in section \ref{sec:rectif} to calculate the vertices of the triangles replacing $f_1$ and $f_2$.

\subsubsection{Perpendicular edges to faces}
\label{sec:perp}


\subsection{Rectification of Intersections}
\label{sec:rectif}


\bibliography{sources} 
\bibliographystyle{ieeetr}

\end{document}