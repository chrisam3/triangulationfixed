# returns true if the two lines lie on the same plane

OnSamePlane:=function(l1,l2,eps)
	if Norm2(Crossproduct(Crossproduct(l1[1]-l1[2],l1[1]-l2[2]),Crossproduct(l1[1]-l1[2],l1[1]-l2[1]))) < eps then
		return true;
	fi;
	return false;
end;

# Computes intersection of two Line Segments in 3D
# https://math.stackexchange.com/questions/270767/find-intersection-of-two-3d-lines
LineSegmentIntersection:=function(l1,l2,eps)
	local C,D,e,f,g,sol,dotsol1,dotsol2;
	C:=l1[1];
	D:=l2[1];
	e:=l1[2]-l1[1];
	f:=l2[2]-l2[1];
	g:=D-C;
	if OnSamePlane(l1,l2,eps) and Norm2(Crossproduct(e,f)) > eps then
		if Dot(Crossproduct(f,g),Crossproduct(f,e)) >= 0. then
			sol := C + Norm2(Crossproduct(f,g))/Norm2(Crossproduct(e,f))*e;
		else
			sol := C - Norm2(Crossproduct(f,g))/Norm2(Crossproduct(e,f))*e;
		fi;
		dotsol1 := Dot(l1[1]-l1[2],sol-l1[2]);
		dotsol2 := Dot(l2[1]-l2[2],sol-l2[2]);
		if Norm2(l1[1]-sol)+Norm2(l1[2]-sol)<=Norm2(l1[1]-l1[2])+eps and Norm2(l2[1]-sol)+Norm2(l2[2]-sol)<=Norm2(l2[1]-l2[2])+eps then
			if not NumericalPosition(l1,sol,eps)<>fail or not NumericalPosition(l2,sol,eps)<> fail then
				return [true,sol];
			fi;
		fi;
	fi;
	
	return [false];
end;

#LineSegmentIntersectionColinear([ [ 1., 0., 0. ], [ 2., 0., 0 ] ],[ [ 1., 0., 0. ], [ 1.6, 0., 0] ],1.e-06);
#LineSegmentIntersectionColinear([ [ 1., 0., 0. ], [ 1.6, 0., 0 ] ],[ [ 1., 0., 0. ], [ 2, 0., 0 ] ],1.e-06);
#LineSegmentIntersectionColinear([ [ 1., 0., 0. ], [ 1.6, 0., 0 ] ],[ [ 1., 0., 0. ], [ 2, 0., 0 ] ],1.e-06);
#LineSegmentIntersectionColinear([ [ 1., 0., 0. ], [ 1.6, 0., 0 ] ],[ [ 1., 0., 0. ], [ 2, 0., 0 ] ],1.e-06);
#LineSegmentIntersectionColinear([ [ 1., 0., 0. ],[ 1.6, 0., 0 ]],[  [ 2., 0., 0 ] ,[ 1., 0., 0. ]],1.e-06);
#LineSegmentIntersectionColinear([ [ 1.6, 0., 1.2 ],[ 1., 0., 0. ] ],[ [ 1., 0., 0. ], [ 2., 0., 2. ] ],1.e-06);

# test for other cases:
choice:=[[ 1.6, 0., 0 ],[ 2., 0., 0 ]];
for i in [1..2] do
	for j in [1..2] do
		for k in [1..2] do
			l1:=[];
			l2:=[];
			l1[i]:=[ 1., 0., 0. ];
			l2[j]:=[ 1., 0., 0. ];
			if BoundPositions(l1)[1]=1 then
				l1[2]:=choice[k];
			else
				l1[1]:=choice[k];
			fi;
			if k=1 then 
				n:=2;
			else 
				n:=1;
			fi;
			if BoundPositions(l2)[1]=1 then
				l2[2]:=choice[n];
			else
				l2[1]:=choice[n];
			fi;
			#Print(LineSegmentIntersectionColinear(l1,l2,eps));
		od;
	od;
od;


LineSegmentIntersectionColinear:=function(l1,l2,eps)
	local v1,v2,x1,x2,y1,y2,r1,r2,s1,s2,i,j;
	l1:=1.*l1;
	l2:=1.*l2;
	v1:=l1[2]-l1[1];
	v2:=l2[2]-l2[1];
	if OnSamePlane(l1,l2,eps) and Norm2(Crossproduct(v1,v2)) < eps and PointsInOneLine(l1[1],l1[2],l2[1],eps) and PointsInOneLine(l1[1],l1[2],l2[2],eps) then

			x1:=l1[1];
			x2:=l2[1];
			y1:=l1[2];
			y2:=l2[2];
			r1:=[];
			r2:=[];
			s1:=[];
			s2:=[];
			for i in [1..3] do
				if Sqrt(v1[i]^2)>eps then
					r1[i]:=(x2[i]-x1[i])/v1[i];
					r2[i]:=(y2[i]-x1[i])/v1[i];
				fi;
				if Sqrt(v2[i]^2)>eps then
					s1[i]:=(x1[i]-x2[i])/v2[i];
					s2[i]:=(y1[i]-x2[i])/v2[i];
				fi;
			od;
			# check if r1 has same non-zero entries
			for i in BoundPositions(r1) do
				for j in BoundPositions(r1) do
					if r1[i]^2>eps and r1[j]^2>eps and (r1[i]-r1[j])^2>eps then
						return [false];
					fi;
				od;
			od;
			for i in BoundPositions(r1) do
				if r1[i]^2>eps then
					r1:=r1[i];
					break;
				fi;
			od;
			if IsList(r1) then
				r1:=0.;
			fi;
			for i in BoundPositions(r2) do
				if r2[i]^2>eps then
					r2:=r2[i];
					break;
				fi;
			od;
			if IsList(r2) then
				r2:=0.;
			fi;
			for i in BoundPositions(s1) do
				if s1[i]^2>eps then
					s1:=s1[i];
					break;
				fi;
			od;
			if IsList(s1) then
				s1:=0.;
			fi;
			for i in BoundPositions(s2) do
				if s2[i]^2>eps then
					s2:=s2[i];
					break;
				fi;
			od;
			if IsList(s2) then
				s2:=0.;
			fi;
			if eps<= r1 and r1 <=1-eps then
				if eps<=s1 and s1<=1-eps then
					return [true,[[x1,x1+r1*v1],[x1+r1*v1,y2-s1*v2],[y2-s1*v2,x2]],1];
				elif eps<=s2 and s2 <=1-eps then
					return [true,[[x1,x1+r1*v1],[x1+r1*v1,x2+s2*v2],[x2+s2*v2,y2]],2];
				fi;
			fi;
			if eps<=r2 and r2 <=1-eps then
				if eps<=s1 and s1 <=1-eps then
					return [true,[[y1,y1-r2*v1],[y1-r2*v1,y2-s1*v2],[y2-s1*v2,x2]],3];
				elif eps<=s2 and s2<=1-eps then
					return [true,[[y1,y1-r2*v1],[y1-r2*v1,x2+s2*v2],[x2+s2*v2,y2]],4];
				fi;
			fi;
			# test if l1 contains l2 and a point of l2

			if FlVEq(x1,x2,eps) then
				#r1=0 and s1=0
				if eps<=r2 and r2 <=1-eps then
					return [true,[[x2,y2],[y2,y1]],5];
				fi;
				if eps<=s2 and s2<=1-eps then
					return [true,[[x1,y1],[y1,y2]],6];
				fi;
			elif FlVEq(y1,x2,eps) then
				#s2=0 and r1=1
				if eps<=r2 and r2 <=1-eps then
					return [true,[[x1,y2],[y2,x2]],7];
				fi;
				if eps<=s1 and s1<=1-eps then
					return [true,[[y1,x1],[x1,y2]],8];
				fi;
			elif FlVEq(y2,x1,eps) then
				#r2=0 and s1=1
				if eps<=r1 and r1 <=1-eps then
					return [true,[[y2,x2],[x2,y1]],9];
				fi;
				if eps<=s2 and s2<=1-eps then
					return [true,[[x2,y1],[y1,x1]],10];
				fi;
			elif FlVEq(y2,y1,eps) then
				#r2=1 and s2=1
				if eps<=r1 and r1 <=1-eps then
					return [true,[[x1,x2],[x2,y2]],11];
				fi;
				if eps<=s1 and s1<=1-eps then
					return [true,[[x2,x1],[x1,y1]],12];
				fi;
			fi;

	fi;
	return [false];
end;;
