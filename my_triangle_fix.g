Read("triangulation.gd");
Read("triangulation.gi");
Read("line_intersection.g");
Read("TwoTriangleIntersection.g");
Read("outer_hull.g");

# global variable
eps:=1.*10^-6;

# returns true when p lies inside the triangle given by a,b,c
# use https://stackoverflow.com/questions/995445/determine-if-a-3d-point-is-within-a-triangle
MyPointInTriangle:=function(a,b,c,p,eps)
local n1,n2,n3;
# if N1=0 or N2=0 or N3=0 check if P lies on the line
#1. the unit normal of triange (A, B, P)  - call it N1
n1:=Crossproduct(p-a,b-a);
if Norm2(n1)<eps then
	if Norm2(p-a)+Norm2(p-b)<=Norm2(a-b)+eps then
		return true;
	else
		return false;
	fi;
else
	n1:=n1/Norm2(n1);
fi;
#2. the unit normal of triangle (B, C, P) - call it N2
n2:=Crossproduct(p-b,c-b);
if Norm2(n2)<eps then
	if Norm2(p-b)+Norm2(p-c)<=Norm2(b-c)+eps then
		return true;
	else
		return false;
	fi;
else
	n2:=n2/Norm2(n2);
fi;
#3.  the unit normal (C,A,P) called N3
n3:=Crossproduct(p-c,a-c);
if Norm2(n3)<eps then
	if Norm2(p-a)+Norm2(p-c)<=Norm2(a-c)+eps then
		return true;
	else
		return false;
	fi;
else
	n3:=n3/Norm2(n3);
fi;

#N1*N2 == 1.0 ?
#N2*N3 == 1.0 ?  
if AbsoluteValue(Dot(n1,n2)-1)<=eps and AbsoluteValue(Dot(n2,n3)-1)<=eps then
	return true;
fi;

return false;

end;;

#calculate_intersections:=function(vof,coordinates,intersections_only)
#	local l,i,j,intersection,intersection2;
#	l:=[];
#	for i in [1..Size(vof)] do
#		l[i]:=[List([1..3],j->coordinates[vof[i][j]]),Set([Set([1,2]),Set([2,3]),Set([3,1])])];
#	od;

#	for i in [1..Size(vof)] do		
#		for j in [i+1..Size(vof)] do
#			# output of the form [vertices,intersection_edges]
#			if i<>j then
#				intersection:=TwoTriangleIntersection(l[i][1]{[1..3]},l[j][1]{[1..3]},eps);
#				#test if both intersection and intersection2 are the same
#				#otherwise join for lines
#				intersection2:=TwoTriangleIntersection(l[j][1]{[1..3]},l[i][1]{[1..3]},eps);
#				if (Size(intersection)=1 or Size(intersection)=0) and Size(intersection2)>0 then
#					
#					if Size(NumericalUniqueListOfLists(Concatenation(intersection,intersection2),eps))=2 then
#						intersection:=NumericalUniqueListOfLists(Concatenation(intersection,intersection2),eps);
#					fi;
#				fi;
#				if Size(intersection)=2 then
#					# add intersection informations to l[i] and l[j]
#					Add(l[i][1],intersection[1]);
#					Add(l[i][1],intersection[2]);
#					Add(l[j][1],intersection[1]);
#					Add(l[j][1],intersection[2]);
#					AddSet(l[i][2],[Size(l[i][1]),Size(l[i][1])-1]);
#					AddSet(l[j][2],[Size(l[j][1]),Size(l[j][1])-1]);
#				fi;
#			fi;
#		od;
#	od;
#	if intersections_only then
#		return l;
#	fi;
#	return join_triangles(List(l,i->triangulate(my_triangle_fix(i))));
#end;;

# return the orthogonal transformation which maps 
# triangle_i onto triangle_j
OrthogonalTransformation:=function(triangle_i,triangle_j)
	return triangle_i^-1*triangle_j;
end;;

# transfer data_triangulated_i to data_triangulated_j via the group action
SymmetryAction:=function(data_triangulated_i,vof_i,vof_j,coordinates,group)
	local group_elem,triangle_j,triangle_i,ort_elem;
	group_elem:=RepresentativeAction(group,vof_i,vof_j,OnSets);	
	triangle_i:=coordinates{vof_i};
	triangle_j:=coordinates{OnTuples(vof_i,group_elem)};
	#find element in O(3) that maps triangle_i onto triangle_j	
	ort_elem:=OrthogonalTransformation(triangle_i,triangle_j);
	return [List(data_triangulated_i[1],i->i*ort_elem),data_triangulated_i[2]];
end;;

#
calculate_intersections:=function(vof,coordinates,intersections_only,group)
	local l,k,i,j,intersection,intersection2,h,orbs_vof,data_triangulated,orb_rep;
	orbs_vof:=Orbits(group,vof,OnSets);
	#TODO for each representative retriangulate
	#TODO put together
	l:=[];
	for i in [1..Size(vof)] do
		l[i]:=[List([1..3],j->coordinates[vof[i][j]]),Set([Set([1,2]),Set([2,3]),Set([3,1])])];
	od;
	#for each representative find self intersections
	for k in [1..Size(orbs_vof)] do
		orb_rep:=orbs_vof[k][1];
		i:=Position(vof,orb_rep);
		for j in [1..Size(vof)] do
			# output of the form [vertices,intersection_edges]
			if i<>j then
				intersection:=TwoTriangleIntersection(l[i][1]{[1..3]},l[j][1]{[1..3]},eps);
				#test if both intersection and intersection2 are the same
				#otherwise join for lines
				intersection2:=TwoTriangleIntersection(l[j][1]{[1..3]},l[i][1]{[1..3]},eps);
				if (Size(intersection)=1 or Size(intersection)=0) and Size(intersection2)>0 then
					
					if Size(NumericalUniqueListOfLists(Concatenation(intersection,intersection2),eps))=2 then
						intersection:=NumericalUniqueListOfLists(Concatenation(intersection,intersection2),eps);
					fi;
				fi;
				if Size(intersection)=2 then
					# add intersection informations only to l[i]
					# l[j] will be obtained later
					Add(l[i][1],intersection[1]);
					Add(l[i][1],intersection[2]);
					#Add(l[j][1],intersection[1]);
					#Add(l[j][1],intersection[2]);
					AddSet(l[i][2],[Size(l[i][1]),Size(l[i][1])-1]);
					#AddSet(l[j][2],[Size(l[j][1]),Size(l[j][1])-1]);
				fi;
			fi;
		od;
	od;
	data_triangulated:=[];
	for k in [1..Size(orbs_vof)] do
		i:=Position(vof,orbs_vof[k][1]);
		data_triangulated[i]:=triangulate(my_triangle_fix(l[i]));
		for h in [2..Size(orbs_vof[k])] do
			j:=Position(vof,orbs_vof[k][h]);
			data_triangulated[j]:=SymmetryAction(data_triangulated[i],vof[i],vof[j],coordinates,group);
		od;
	od;
	eps:=Sqrt(eps);
	return join_triangles(data_triangulated);
end;;

clean_data:=function(data,eps)
	local map,i,new_edges,new_vertices,map2;
	data[1]:=1.*data[1];
	# delete multiple occurences of vertices
	map:=[];
	for i in [1..Size(data[1])] do
		map[i]:=NumericalPosition(data[1],data[1][i],eps);
	od;
	new_vertices:=[];
	map2:=[];
	for i in [1..Size(data[1])] do
		if i in map then
			Add(new_vertices,data[1][i]);
			map2[i]:=Size(new_vertices);
		else
			map2[i]:=map2[map[i]];
		fi;
	od;
	new_edges:=List(data[2],j->[map2[j[1]],map2[j[2]]]);
	data[2]:=new_edges;
	data[1]:=new_vertices;
	# delete multiple occurences of edges
	data[2]:=Set(List(data[2],i->Set(i)));
	return data;
end;;

triangulate:=function(data)
	local e,faces,is_triangle,i,j;
	faces:=[];
	data[2]:=Set(List(data[2],i->Set(i)));
	for e in data[2] do
		for i in [1..Size(data[1])] do
			if Set([i,e[1]]) in data[2] and Set([i,e[2]]) in data[2] then
				is_triangle:=true;
				for j in [1..Size(data[1])] do
					if j<>i and j<>e[1] and j<>e[2] and MyPointInTriangle(
					data[1][e[1]],data[1][e[2]],data[1][i],data[1][j],eps) then
						is_triangle:=false;
					fi;
				od;
				if is_triangle then
					Add(faces,[e[1],e[2],i]);
				fi;
			fi;
		od;
	od;
	return [data[1],Set(List(faces,i->Set(i)))];
end;;

# join the data of all triangles
join_triangles:=function(list)
	local vertices,VerticesOfFaces,map,i,pos,data;
	vertices:=[];
	VerticesOfFaces:=[];
	for data in list do
		#first add all the vertices of data[1] into vertices and 
		#create a labeling map for the vertices used in data[2]
		map:=[];
		for i in [1..Size(data[1])] do
			pos:=NumericalPosition(vertices,data[1][i],eps);
			if pos=fail then
				Add(vertices,data[1][i]);
				map[i]:=Size(vertices);
			else
				map[i]:=pos;
			fi;
		od;
		VerticesOfFaces:=Concatenation(VerticesOfFaces,List(data[2],triangle->[map[triangle[1]],map[triangle[2]],map[triangle[3]]]));
	od;
	return [vertices,VerticesOfFaces];
end;;


is_not_triangulated:=function(data)
	local e,triangular_edges;
	triangular_edges:=[];
	data[2]:=Set(List(data[2],i->Set(i)));
	for e in data[2] do
		for i in [1..Size(data[1])] do
			if Set([i,e[1]]) in data[2] and Set([i,e[2]]) in data[2] then
				Add(triangular_edges,e);
				break;
			fi;
		od;
	od;
	if Set(triangular_edges)=data[2] then
		return false;
	fi;
	return true;
end;;


# test 
# fix_intersections_planar([[[1.,0,0],[2.,0,0],[1.,0,1]],[]]);
# fix_intersections_planar([[[1.,0,0],[2.,0,0],[1.,0,1]],[[1,2],[2,3],[3,1]]]);
# fix_intersections_planar([[[1.,0,0],[3.,0,0],[1.,0,1],[2.,0,0]],[[1,2],[2,3],[3,1]]]);
# fix_intersections_planar([[[1.,0,0],[3.,0,0],[1.,0,4],[2.,0,0],[1.,1.,1]],[[1,2],[2,3],[3,1],[4,5]]]);
# fix_intersections_planar([[[1.,0,0],[3.,0,0],[1.,0,4],[2.,0,0],[1.,0.,3]],[[1,2],[2,3],[3,1],[4,5]]]);
# fix_intersections_planar([[[1.,0,0],[4.,0,0],[1.,0,4],[2.,0,0],[1.,0.,3],[2,0,2]],[[1,2],[2,3],[3,1],[4,5],[1,6]]]);

fix_intersections_planar:=function(data)
	local i,j,res,entry,entry1,entry2,entry_in_i,entry_in_j,addy,check_edges,orig_j,orig_i,cur,k;
	# check for non-paralel intersecting lines
		data:=clean_data(data,eps);
		check_edges:=Combinations([1..Size(data[2])],2);;
		while check_edges<>[] do
				cur:=Remove(check_edges,1);
				i:=cur[1];
				j:=cur[2];
				if i <=Size(data[2]) and j<=Size(data[2]) then
					res:=LineSegmentIntersection([data[1][data[2][i][1]],data[1][data[2][i][2]]],[data[1][data[2][j][1]],data[1][data[2][j][2]]],eps);
					if res[1] then
						#Print("Edge ",i,": ",data[2][i]," intersects with Edge ",j,": ",data[2][j],"\n");
						#check if res[2] is already in data[1]
						entry:=NumericalPosition(data[1],res[2],eps);
						if entry <> fail then
							#check if res[2] is part of i or j
							entry_in_i:=false;
							if entry in data[2][i] then
								entry_in_i:=true;
							fi;
							entry_in_j:=false;
							if entry in data[2][j] then
								entry_in_j:=true;
							fi;
							#split edges
							if entry_in_i and not entry_in_j then
								#Print("case i");
								#Error();
								# split j	
								orig_j:=ShallowCopy(data[2][j]);	
								data[2]:=Union(data[2],Set([Set([data[2][j][1],entry]),Set([data[2][j][2],entry])]));				
								#AddSet(data[2],Set([data[2][j][1],entry]));
								#AddSet(data[2],Set([data[2][j][2],entry]));
								RemoveSet(data[2],orig_j);
							elif not entry_in_i and entry_in_j then
								#Print("case j");
								#Error();
								# split i	
								orig_i:=ShallowCopy(data[2][i]);
								data[2]:=Union(data[2],Set([Set([data[2][i][1],entry]),Set([data[2][i][2],entry])]));	
								#AddSet(data[2],Set([data[2][i][1],entry]));
								#AddSet(data[2],Set([data[2][i][2],entry]));
								RemoveSet(data[2],orig_i);
							elif not entry_in_i and not entry_in_j then
								#Print("in vertices but not case i and case j");
								#Error();
								orig_i:=ShallowCopy(data[2][i]);
								orig_j:=ShallowCopy(data[2][j]);
								data[2]:=Union(data[2],Set([Set([data[2][i][1],entry]),Set([data[2][i][2],entry]),Set([data[2][j][1],entry]),Set([data[2][j][2],entry])]));
								RemoveSet(data[2],orig_i);
								RemoveSet(data[2],orig_j);	
								#AddSet(data[2],Set([data[2][i][1],entry]));
								#AddSet(data[2],Set([data[2][i][2],entry]));
								#AddSet(data[2],Set([data[2][j][1],entry]));
								#AddSet(data[2],Set([data[2][j][2],entry]));
							fi;
						else
							#Print("case fail");
							#	Error();
							Add(data[1],res[2]);
							entry:=Size(data[1]);
							orig_i:=ShallowCopy(data[2][i]);
							orig_j:=ShallowCopy(data[2][j]);
							#AddSet(data[2],Set([data[2][i][1],entry]));
							#AddSet(data[2],Set([data[2][i][2],entry]));
							#AddSet(data[2],Set([data[2][j][1],entry]));
							#AddSet(data[2],Set([data[2][j][2],entry]));
							data[2]:=Union(data[2],Set([Set([data[2][i][1],entry]),Set([data[2][i][2],entry]),Set([data[2][j][1],entry]),Set([data[2][j][2],entry])]));
							RemoveSet(data[2],orig_i);
							RemoveSet(data[2],orig_j);
						fi;
						check_edges:=Combinations([1..Size(data[2])],2);;			
					fi;
				fi;
				for k in [1..Size(data[2])] do
					if Size(data[2][k])=1 then
						Error();
					fi;
				od;
		od;
		data:=clean_data(data,eps);
		
		check_edges:=Combinations([1..Size(data[2])],2);;
		# check for colinear lines
		while check_edges<>[] do
			cur:=Remove(check_edges,1);
			i:=cur[1];
			j:=cur[2];
				if i <=Size(data[2]) and j<=Size(data[2]) then
					res:=LineSegmentIntersectionColinear([data[1][data[2][i][1]],data[1][data[2][i][2]]],[data[1][data[2][j][1]],data[1][data[2][j][2]]],eps);
					if res[1] then
						#Print("Edge ",i,": ",data[2][i]," intersects with Edge ",j,": ",data[2][j],"\n");
						#Error();
						if res[3]<=4 then
							entry1:=NumericalPosition(data[1],res[2][1][2],eps);
							entry2:=NumericalPosition(data[1],res[2][2][2],eps);
							if entry1= fail then
								Add(data[1],res[2][1][2]);
								entry1:=Size(data[1]);
							fi;
							if entry2 = fail then
								Add(data[1],res[2][2][2]);
								entry2:=Size(data[1]);
							fi;
						fi;
						orig_i:=ShallowCopy(data[2][i]);
						orig_j:=ShallowCopy(data[2][j]);
						addy:=Set([]);
						if res[3]=1 then
							addy:=Set([]);
							AddSet(addy,Set([data[2][i][1],entry1]));
							AddSet(addy,Set([entry1,entry2]));
							AddSet(addy,Set([entry2,data[2][j][1]]));
							data[2]:=Union(data[2],addy);
						elif res[3]=2 then 
							addy:=Set([]);
							AddSet(addy,Set([data[2][i][1],entry1]));
							AddSet(addy,Set([entry1,entry2]));
							AddSet(addy,Set([entry2,data[2][j][2]]));
							data[2]:=Union(data[2],addy); 
						elif res[3]=3 then 
							addy:=Set([]);
							AddSet(addy,Set([data[2][i][2],entry1]));
							AddSet(addy,Set([entry1,entry2]));
							AddSet(addy,Set([entry2,data[2][j][1]]));
							data[2]:=Union(data[2],addy); 
						elif res[3]=4 then 
							addy:=Set([]);
							AddSet(addy,Set([data[2][i][2],entry1]));
							AddSet(addy,Set([entry1,entry2]));
							AddSet(addy,Set([entry2,data[2][j][2]]));
							data[2]:=Union(data[2],addy); 
						elif res[3]=5 then 
							addy:=Set([]);
							AddSet(addy,Set([data[2][j][1],data[2][j][2]]));
							AddSet(addy,Set([data[2][j][2],data[2][i][2]]));
							data[2]:=Union(data[2],addy); 
						elif res[3]=6 then 
							addy:=Set([]);
							AddSet(addy,Set([data[2][i][1],data[2][i][2]]));
							AddSet(addy,Set([data[2][j][2],data[2][i][2]]));
							data[2]:=Union(data[2],addy); 
						elif res[3]=7 then 
							addy:=Set([]);
							AddSet(addy,Set([data[2][i][1],data[2][j][2]]));
							AddSet(addy,Set([data[2][j][1],data[2][j][2]]));
							data[2]:=Union(data[2],addy); 
						elif res[3]=8 then 
							addy:=Set([]);
							AddSet(addy,Set([data[2][i][1],data[2][j][2]]));
							AddSet(addy,Set([data[2][i][1],data[2][i][2]]));
							data[2]:=Union(data[2],addy); 
						elif res[3]=9 then 
							addy:=Set([]);
							AddSet(addy,Set([data[2][i][2],data[2][j][1]]));
							AddSet(addy,Set([data[2][j][1],data[2][j][2]]));
							data[2]:=Union(data[2],addy); 
						elif res[3]=10 then 
							addy:=Set([]);
							AddSet(addy,Set([data[2][i][2],data[2][j][1]]));
							AddSet(addy,Set([data[2][i][1],data[2][i][2]]));
							data[2]:=Union(data[2],addy); 
						elif res[3]=11 then 
							addy:=Set([]);
							AddSet(addy,Set([data[2][i][1],data[2][i][2]]));
							AddSet(addy,Set([data[2][j][1],data[2][j][2]]));
							data[2]:=Union(data[2],addy); 
						elif res[3]=12 then 
							addy:=Set([]);
							AddSet(addy,Set([data[2][j][1],data[2][i][1]]));
							AddSet(addy,Set([data[2][i][1],data[2][i][2]]));
							data[2]:=Union(data[2],addy);
						fi;
						if addy=Set([]) then
							RemoveSet(data[2],orig_i);
							RemoveSet(data[2],orig_j);
						elif orig_i in addy then
							RemoveSet(data[2],orig_j);
						elif orig_j in addy then
							RemoveSet(data[2],orig_i);
						elif not orig_i in addy and not orig_j in addy then
							RemoveSet(data[2],orig_i);
							RemoveSet(data[2],orig_j);
						fi;



						check_edges:=Combinations([1..Size(data[2])],2);;
					fi;
				fi;
		od;
		data:=clean_data(data,eps);

		check_edges:=[1..Size(data[2])];
		#check if vertices split edges
		while check_edges<>[] do
			j:=Remove(check_edges);
			for i in [1..Size(data[1])] do
				
		       	if j<=Size(data[2]) then
					if LineSegmentIntersectionColinear([data[1][i],data[1][data[2][j][1]]],[data[1][data[2][j][1]],data[1][data[2][j][2]]],eps)[1] and LineSegmentIntersectionColinear([data[1][i],data[1][data[2][j][2]]],[data[1][data[2][j][1]],data[1][data[2][j][2]]],eps)[1] and not i in data[2][j] then
						if Size(Set([data[2][j][1],i]))=1 or Size(Set([data[2][j][2],i]))=1 then
							Error();
						fi;
						orig_j:=ShallowCopy(data[2][j]);
						data[2]:=Union(data[2],Set([Set([data[2][j][1],i]),Set([data[2][j][2],i])]));
						RemoveSet(data[2],orig_j);
						check_edges:=[1..Size(data[2])];
					fi;
				fi;
			od;	
		od;
	return clean_data(data,eps);;
end;;
	
	
#4754
# data contains vertices, edges
my_triangle_fix:=function(data)
	local map,i,edge,poss_edges,no_poss_edges,new_edges,ok,e,res;
	
	data:=clean_data(data,eps);
	data:=fix_intersections_planar(data);


	poss_edges:=[];
	for i in [1..Size(data[1])] do
		for j in [i+1..Size(data[1])] do
			ok:=true;
			for e in [1..Size(data[2])] do
				res:=LineSegmentIntersection([data[1][i],data[1][j]],[data[1][data[2][e][1]],data[1][data[2][e][2]]],eps);

				if (res[1] and (not NumericalPosition([data[1][i],data[1][j]],res[2],eps)<>fail or not NumericalPosition([data[1][data[2][e][1]],data[1][data[2][e][2]]],res[2],eps)<>fail)) or LineSegmentIntersectionColinear([data[1][i],data[1][j]],[data[1][data[2][e][1]],data[1][data[2][e][2]]],eps)[1] then
					ok:=false;
					break;
				fi;
			od;
			if ok and not Set([i,j]) in data[2] then
				if Norm2(data[1][i]-data[1][j])>eps then
					Add(poss_edges,[Set([i,j]),Norm2(data[1][i]-data[1][j])]);
				fi;
			fi;
		od;
	od;
	SortBy(poss_edges,i->-i[2]);
	new_edges:=Set([]);
	while poss_edges<>[] do
		edge:=Remove(poss_edges);
		AddSet(new_edges,edge[1]);
		data[2]:=Union(data[2],Set([edge[1]]));
		for e in [1..Size(new_edges)] do
			no_poss_edges:=[];
			for i in [1..Size(poss_edges)] do
				res:=LineSegmentIntersection([data[1][poss_edges[i][1][1]],data[1][poss_edges[i][1][2]]],[data[1][new_edges[e][1]],data[1][new_edges[e][2]]],eps);

				if (res[1] and (not NumericalPosition([data[1][poss_edges[i][1][1]],data[1][poss_edges[i][1][2]]],res[2],eps)<>fail or not NumericalPosition([data[1][new_edges[e][1]],data[1][new_edges[e][2]]],res[2],eps)<>fail)) or LineSegmentIntersectionColinear([data[1][poss_edges[i][1][1]],data[1][poss_edges[i][1][2]]],[data[1][new_edges[e][1]],data[1][new_edges[e][2]]],eps)[1] then
					Add(no_poss_edges,i);
				fi;
			od;
			SortBy(no_poss_edges,i->-i);
			for i in no_poss_edges do
				Remove(poss_edges,i);
			od;
		od;
	od;
	
	return clean_data(data,eps);
end;



	