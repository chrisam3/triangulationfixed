# double 6-Gon

points:=[[0,0,0],
		[1,0,0],
		[1.5,-Sqrt(3.)/2,0],
		[1,-Sqrt(3.),0],
		[0,-Sqrt(3.),0],
		[-0.5,-Sqrt(3.)/2,0],
		[0.5,-Sqrt(3.)/2,1],
		[0.5,-Sqrt(3.)/2,-1]];

faces:=[[1,2,7],[2,3,7],[3,4,7],[4,5,7],[5,6,7],[6,1,7],  [1,2,8],[2,3,8],[3,4,8],[4,5,8],[5,6,8],[6,1,8]
 ];

# join 4-copies to create Ramified Edges

translations:=[[1.5,Sqrt(3.)/2,0],[1.5,-Sqrt(3.)/2,0],[3,0,0],[0,0,0]];

new_faces:=[];
new_points:=[];
for t in [1..Size(translations)] do
	new_faces:=Concatenation(new_faces,faces+Size(points)*(t-1));
	new_points:=Concatenation(new_points,points+translations[t]);
od;
t:=TriangularComplexByVerticesInFaces(new_faces);
data:=clean_data([new_points,VerticesOfEdges(t)],eps);
data:=triangulate(data);
points:=data[1];
t:=TriangularComplexByVerticesInFaces(data[2]);
